<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// TODO CREAR UNA PAGINA DE INICIO COMO TAL

// use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Route;

Route::get('/', 'LandingPageController@home');
// Route::get('/home', 'LandingPageController@home');
Route::get('/home', 'PartidaController@getPartida')->name("home");


/* PARTIDAS */
Route::group(['middleware' => 'auth'], function()
{
    Route::prefix('partidas')->group(function () {
        // Route::get('/', 'PartidaController@getPartida')->name("home");
        Route::get('/privadas', 'PartidaController@getPartidas')->name('partidas.listado');

        Route::get('/publicas', 'PartidaController@getPartidasPublicas')->name('partidas.publicas');
        
        Route::get('/crear', 'PartidaController@getCrearPartida')->name("partidas.crear");
        Route::post('/crear', 'PartidaController@postCrearPartida')->name("partidas.crear");
        
        Route::get('/invitaciones', 'PartidaController@getInvitaciones')->name("partidas.invitaciones");
       
        // Route::get('/buscarAmigos', 'PartidaController@getAmigos');
        Route::post('/buscarAmigos', 'PartidaController@postAmigos');
        Route::post('/invitarAmigos', 'PartidaController@postSelectAmigos');
    } );
});

/* RONDAS */
Route::group(['middleware' => 'auth'], function()
{
    Route::prefix('partida')->group(function () {
        Route::get('{partida}/rondas', 'RondasController@getRondas')->name("rondas.listado");
        Route::get('{partida}/rondas/crear', 'RondasController@getCrearRondas')->name('rondas.tema');
        Route::post('{partida}/rondas/crear', 'RondasController@postCrearTema');
    });
});

/* DIBUJOS */
Route::group(['middleware' => 'auth'], function()
{
    Route::prefix('partida/{partida}/ronda')->group(function () {
        Route::get('{ronda}/subir', 'DibujosController@getSubirDibujos')->name('rondas.dibujos');
        Route::get('{ronda}/ver', 'DibujosController@getVerDibujosRonda')->name('dibujos.listado');

        // Route::get('{ronda}/ganador', 'DibujosController@getGanador');
        
        Route::get('{ronda}/ganador', 'DibujosController@getGanador')->name('dibujos.ganadores');
        
        Route::post('{ronda}/voto/{dibujo}', 'DibujosController@postVotos')->name('dibujos.voto');//'DibujosController@postVotos'
    });
    Route::prefix('dibujos')->group(function () {
        Route::get('ver/{id}', 'DibujosController@getVer')->where('id','[0-9]+');
    });
});

Route::group(['middleware' => 'auth'], function()
{   
    Route::prefix('partida/{partida}/ronda/{ronda}/dibujo')->group(function () {
        Route::get('subir', 'DibujosController@getCrear')->name('dibujos.subir');
        Route::post('subir', 'DibujosController@postCrear')->name('dibujos.subir');
        Route::get('pizarra', 'DibujosController@getPizarra')->name('dibujos.pizarra');
    });
});

/* USUARIOS */
Auth::routes(['verify' => true]);
Route::get('logout', 'Auth\LoginController@logout');

// PERFIL
Route::group(['middleware' => 'auth'], function()
{
    Route::prefix('perfil')->group(function () {
        Route::get('/', 'PerfilController@perfil');
        Route::get('/editar', 'PerfilController@getEditar');
        Route::post('/editar', 'PerfilController@postEditar');
        Route::get('/cambio','PerfilController@getCambioPass');
        Route::post('/cambio','PerfilController@postCambioPass');
    // editarPerfil
    });
});

/* AMIGOS */

Route::group(['middleware' => 'auth'], function()
{
    Route::get('amigos', 'AmigosController@getAmigos');
    Route::get('amigos/solicitudes', 'AmigosController@getSolicitudes');
    Route::post('amigos/solicitudes/eliminar', 'AmigosController@postEliminarSolicitud');
    Route::post('amigos/solicitudes/aceptar', 'AmigosController@postAceptarSolicitud');
    Route::get('amigos/busqueda', 'AmigosController@getBusquedaUsuarios');
    Route::post('amigos/busqueda/peticion', 'AmigosController@postPeticion');
    // Route::post('amigos/{id}', 'AmigosControlles@getAmigos');

    // AJAX
    Route::post('amigos/solicitudes/ajaxBuscarAmigo', 'AmigosController@ajaxBuscarAmigo');
});
