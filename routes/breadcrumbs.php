<?php
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('home'));
});

// Home > Crear Partida
Breadcrumbs::for('crearPartidas', function ($trail) {
    $trail->parent('home');
    $trail->push('Crear Partida', route('partidas.crear'));
});

// Home >  Mis Partidas
Breadcrumbs::for('partidas', function ($trail) {
    $trail->parent('home');
    $trail->push('Mis Partidas', route('partidas.listado'));
});

// Home >  Mis Partidas
Breadcrumbs::for('partidasPublicas', function ($trail) {
    $trail->parent('home');
    $trail->push('Partidas publicas', route('partidas.publicas'));
});

// Home > Mis Partidas > [partida_id]
Breadcrumbs::for('rondas', function ($trail, $partida) {
    $anterior = ($partida->privada) ? 'partidas': 'partidasPublicas';
    $trail->parent($anterior);
    $trail->push('Partida ' . $partida->slug , route('rondas.listado', $partida));
});

// Home > Mis Partidas > Partida [partida_id] > rondas
Breadcrumbs::for('temas', function ($trail, $partida) {
    $anterior = ($partida->privada) ? 'partidas': 'partidasPublicas';
    $trail->parent($anterior);
    $trail->push('Tema ' . $partida->slug , route('rondas.tema', $partida));
});

// Home > Mis Partidas > Partida [partida_id] > Ronda  [ronda_id]
Breadcrumbs::for('rondasDibujos', function ($trail, $ronda) {
    $trail->parent('rondas', $ronda->partida);
    $trail->push('Ronda ' . $ronda->slug , route('rondas.dibujos', [$ronda->partida, $ronda]));
});

// Home > Mis Partidas > Partida [partida_id] > Ronda  [ronda_id] > listado
Breadcrumbs::for('dibujosListado', function ($trail, $ronda) {
    $trail->parent('rondas', $ronda->partida);
    $trail->push('Dibujos ronda ' . $ronda->slug , route('dibujos.listado', [$ronda->partida, $ronda]));
});

// Home > Mis Partidas > Partida [partida_id] > Ronda  [ronda_id] > listado
Breadcrumbs::for('dibujosGanador', function ($trail, $ronda) {
    $trail->parent('rondas', $ronda->partida);
    $trail->push('Ronda Completada', route('dibujos.ganadores', [$ronda->partida, $ronda]));
});

// Home > Mis Partidas > Partida [partida_id] > Ronda  [ronda_id] > subir
Breadcrumbs::for('subirDibujos', function ($trail, $ronda) {
    $trail->parent('rondasDibujos', $ronda);
    $trail->push('Subir dibujo', route('dibujos.subir', [$ronda->partida, $ronda]));
});

// Home > Mis Partidas > Partida [partida_id] > Ronda  [ronda_id] > pizarra
Breadcrumbs::for('pizarraDibujos', function ($trail, $ronda) {
    $trail->parent('rondasDibujos',  $ronda);
    $trail->push('Pizarra Interactiva', route('dibujos.pizarra', [$ronda->partida, $ronda]));
});

// Home > Mis Partidas > Partida [partida_id] > Ronda  [ronda_id] > voto
Breadcrumbs::for('dibujoVoto', function ($trail, $ronda, $dibujo) {
    $trail->parent('dibujosListado',  $ronda);
    $trail->push('Votación', route('dibujos.voto', [$ronda->partida, $ronda, $dibujo]));
});



// Home > Invitaciones
Breadcrumbs::for('invitaciones', function ($trail) {
    $trail->parent('home');
    $trail->push('Invitaciones', route('partidas.invitaciones'));
});

// Home > Blog > [Category]
Breadcrumbs::for('category', function ($trail, $category) {
    $trail->parent('blog');
    $trail->push($category->title, route('category', $category->id));
});

// Home > Blog > [Category] > [Post]
Breadcrumbs::for('post', function ($trail, $post) {
    $trail->parent('category', $post->category);
    $trail->push($post->title, route('post', $post->id));
});