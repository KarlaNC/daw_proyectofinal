    @forelse($usuario_encontrados as $usuario)
        <div class="col-md-6 p-0">
            <div class="bloque-amigo">
                <div class="row">
                    <div class="col-lg-7 col-md-12">
                        <h5 class="mb-0">{{ $usuario->name }}</h5>
                        <p class="text-uppercase text-muted">{{ $usuario->name}}</p>
                        <div class="row">
                            <div class="col-6">
                                <form action="{{ action('AmigosController@postPeticion') }}" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <button class="btn btn-sm btn-solucitud btn-degradado">Añadir</button>
                                    <input type="hidden" name="solicitud_id" value="{{ $usuario->id }}">
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-12">
                        <div class="avatar-circulo avatar-3 rounded-circle" style="background-image: url({{ asset('storage/'.$usuario->avatar) }});"></div>
                    </div>
                </div>
            </div>
        </div>
    @empty
        <div class="col">
            No tiene ninguna solicitud pendiente
        </div>
    @endforelse
