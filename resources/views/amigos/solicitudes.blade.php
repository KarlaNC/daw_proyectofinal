@extends('layouts.master')

@section('titulo')
Index
@endsection


@section('contenido')

@if (session('mensaje'))
    <div class="alert alert-success" role="alert">
        {{ session('mensaje') }}
    </div>
@endif

{{-- LISTADO AMIGOS USUARIO --}}
<div class="container">
    <div class="row mt-5 cuadrado-x">
        <div class="col-md-4 cuadrado-izq">
            <ul>
                <li class="li-izq pointer" tabindex="0" onclick="window.location='{{ url('/amigos') }}'">
                    Mis Amigos
                </li>
                <li class="li-izq f-bold  pointer" style="border-color: #f05a70" tabindex="0" onclick="window.location='{{ url('/amigos/solicitudes') }}'">
                    Solicitudes <span class="badge badge-primary"> {{count($solicitudes_usuario)}} </span>
                </li>
                <li class="li-izq pointer"  tabindex="0" onclick="window.location='{{ url('/amigos/busqueda') }}'">
                    Buscar Amigo
                </li>
            </ul>
        </div>
        <div class="col-md-8 cuadraro-der">
            <div class="row mb-3">
                <label for="name" class="col-sm-12 col-form-label f-bold text-center">Amigos</label>
            </div>
            <div class="row form-group">
                @if(@empty($solicitudes_usuario) )
                    <div class="col">
                        No tiene ninguna solicitud pendiente
                    </div>
                @else
                    @foreach ($solicitudes_usuario as $usuario)
                        <div class="col-md-6 p-0">
                            <div class="bloque-amigo">
                                <div class="row">
                                    <div class="col-lg-7 col-md-12">
                                        <h5 class="mb-0">{{ $usuario->nick }}</h5><!-- Nick -->
                                        <p class="text-uppercase text-muted">{{ $usuario->name }}</p><!-- Nombre -->
                                        <div class="row">
                                            @if($usuario->user_1_id != Auth::user()->id)
                                                <div class="col-6">
                                                    {{-- TODO: agregar amigos --}}
                                                    <form action="{{ action('AmigosController@postAceptarSolicitud') }}" method="POST" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <button class="btn btn-primary btn-sm btn-solucitud">Confirmar</button>
                                                        <input type="hidden" name="solicitud_id" value="{{ $usuario->solicitud_id }}">
                                                    </form>
                                                </div>
                                            @endif
                                            <div class="col-6">
                                                <form action="{{ action('AmigosController@postEliminarSolicitud') }}" method="POST" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <button class="btn btn-sm btn-solucitud btn-degradado">Eliminar</button>

                                                    <input type="hidden" name="solicitud_id" value="{{ $usuario->solicitud_id }}">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-12">
                                        <div class="avatar-circulo avatar-3 rounded-circle" style="background-image: url({{ asset('storage/'.$usuario->avatar) }});"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
