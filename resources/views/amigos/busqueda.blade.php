@extends('layouts.master')

@section('titulo')
Index
@endsection


@section('contenido')

@if (session('mensaje'))
    <div class="alert alert-success" role="alert">
        {{ session('mensaje') }}
    </div>
@endif

{{-- LISTADO AMIGOS USUARIO --}}
<div class="container">
    <div class="row mt-5 cuadrado-x">
        <div class="col-md-4 cuadrado-izq">
            <ul>
                <li class="li-izq pointer" tabindex="0" onclick="window.location='{{ url('/amigos') }}'">
                    Mis Amigos
                </li>
                <li class="li-izq pointer" tabindex="0" onclick="window.location='{{ url('/amigos/solicitudes') }}'">
                    Solicitudes <span class="badge badge-primary"> {{count($solicitudes_usuario)}} </span>
                </li>
                <li class="li-izq f-bold pointer" style="border-color: #f05a70" tabindex="0" onclick="window.location='{{ url('/amigos/busqueda') }}'">
                    Buscar Amigo
                </li>
            </ul>
        </div>
        <div class="col-md-8 cuadraro-der">
            <div class="row mb-3">
                <label for="name" class="col-sm-12 col-form-label f-bold text-center">Buscar amigos</label>
            </div>
            <div class="row form-group">
                <input type="text" class="form-control" placeholder="" id="buscar_amigos">
                <input type="hidden" id="urlBuscarAmigo" value="{{ action('AmigosController@ajaxBuscarAmigo') }}">
                <input type="hidden" id="token" value="{{ csrf_token() }}">
            </div>
            <div class="row form-group" id="amigos_encontrados">
                {{-- contenedor amigos --}}
            </div>
        </div>
    </div>
</div>
@endsection
