@extends('layouts.master')

@section('titulo')
Index
@endsection


@section('contenido')

@if (session('mensaje'))
    <div class="alert alert-success" role="alert">
        {{ session('mensaje') }}
    </div>
@endif

{{-- LISTADO AMIGOS USUARIO --}}
<div class="container">
    <div class="row mt-5 cuadrado-x">
        <div class="col-md-4 cuadrado-izq">
            <ul>
                <li class="li-izq f-bold pointer" style="border-color: #f05a70" tabindex="0" onclick="window.location='{{ url('/amigos') }}'">
                    Mis Amigos
                </li>
                <li class="li-izq pointer" tabindex="0" onclick="window.location='{{ url('/amigos/solicitudes') }}'">
                    {{-- Solicitudes <span class="badge badge-primary"> Primary </span> --}}
                    Solicitudes <span class="badge badge-primary"> {{count($solicitudes_usuario)}} </span>
                </li>
                <li class="li-izq pointer" tabindex="0" onclick="window.location='{{ url('/amigos/busqueda') }}'">
                    Buscar Amigo
                </li>
            </ul>
        </div>
        <div class="col-md-8 cuadraro-der">
            {{-- ¿ Dejar From ? --}}
            <form action="{{ action('PerfilController@postEditar') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row mb-3">
                    <label for="name" class="col-sm-12 col-form-label f-bold text-center">Amigos</label>
                </div>
                <div class="row form-group">
                    @forelse ($amigos_usuario as $amigo)
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    
                                    
                                    
                                    <div class="media-body overflow-hidden">
                                        <div class="row">
                                            <div class="col-lg-7 col-md-12">
                                                <h5 class="card-text mb-0">{{ $amigo->nick }}</h5>
                                                <p class="card-text text-uppercase text-muted">{{ $amigo->name }}</p>
                                                <p class="card-text">
                                                    {{ $amigo->email }}
                                                </p>
                                            </div>
                                            <div class="col-lg-5 col-md-12">
                                                <div class="avatar-circulo avatar-select rounded-circle float-right avatar-3" style="background-image: url({{ asset('storage/'.$amigo->avatar) }});"> </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <a href="#" class="tile-link"></a>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="col">
                            Aún no tienes ningún amigos
                        </div>
                    @endforelse
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
