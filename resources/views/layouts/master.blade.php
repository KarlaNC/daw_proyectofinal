<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}">

        {{-- Boostrap --}}
        <link rel="stylesheet" href="{{ url('/assets/font-awesome-4.7.0/css/font-awesome.min.css') }}">

        {{-- Estilos propios --}}
        {{-- <link rel="stylesheet" href="{{ url('/css/estilo_landing_page.css') }}"> --}}
        <link rel="stylesheet" href="{{ url('/css/principal.css') }}">
        <link rel="stylesheet" href="{{ url('/css/nav-bar.css') }}">
        <link rel="stylesheet" href="{{ url('/css/tabla_bordes.css') }}">
        <link rel="stylesheet" href="{{ url('/css/auth.css') }}">

        {{-- <link rel="stylesheet" href="{{ url('/css/estilo_perfil.css') }}"> --}}

        {{-- FavIcon --}}
        <link rel="apple-touch-icon" sizes="180x180" href="{{ url('/assets/favicon_io/fav.svg') }}">
        <link rel="icon" sizes="32x32" href="{{ url('/assets/favicon_io/fav.svg') }}">
        <link rel="icon" sizes="16x16" href="{{ url('/assets/favicon_io/fav.svg') }}">
        <link rel="manifest" href="{{ url('/assets/favicon_io/site.webmanifest') }}">

        {{-- Font Family --}}
        <link href="https://fonts.googleapis.com/css2?family=Quicksand&display=swap" rel="stylesheet">
	    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

        {{-- PIZARRA JS --}}
	    <link href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/3.6.95/css/materialdesignicons.css" rel="stylesheet">
        {{-- <link rel="icon" sizes="16x16" href="{{ url('/assets/pizarra/materialdesignicons.css') }}"> --}}

	    {{-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script> --}}
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        


        {{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script> --}}

        {{-- JS personalizado por cada controlador --}}
        <?php if(public_path() == '/var/www/html/krylic/public') :?>
            <?php if (file_exists(public_path() . '/js/' . strtolower($controller) . '/custom.js')): ?>
                <script src="{{ url('/js/' . strtolower($controller) . '/custom.js') }}"></script>
            <?php endif; ?>
        <?php else: ?>
            <?php if(file_exists(public_path() . '\js\\' . strtolower($controller) . '\custom.js')): ?>
                <script src="{{ url('/js/' . strtolower($controller) . '/custom.js') }}"></script>
                <script>
                    // URL para ajax o llamadas desde custom.js
                    var url_global = "{{url('/')}}";
                </script>
            <?php endif; ?>
        <?php endif; ?>
        
        {{-- CSS personalizado por cada controlador --}}
        <?php if(public_path() == '/var/www/html/krylic/public') :?>
            <?php if (file_exists(public_path() . '/css/' . strtolower($controller) . '/custom.css')): ?>
                <link rel="stylesheet" href="{{ url('/css/' . strtolower($controller) . '/custom.css') }}">
            <?php endif; ?>
        <?php else: ?>
            <?php if (file_exists(public_path() . '\css\\' . strtolower($controller) . '\custom.css')): ?>
                <link rel="stylesheet" href="{{ url('/css/' . strtolower($controller) . '/custom.css') }}">
            <?php endif; ?>
        <?php endif; ?>

        <title>Krylic | <?= $controller ?? '' ?></title>
    </head>
  <body>
        @if (Auth::user())
            @include('partials.navbar')
        @endif
        <div class="main">
            @yield('contenido')
        </div>
        
        {{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> --}}
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    </body>
@if (Auth::user())
    @include('partials.footer')
@endif
</html>
