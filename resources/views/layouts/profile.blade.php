<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    {{-- Boostrap --}}
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    {{-- Estilos propios --}}
    <link rel="stylesheet" href="{{ url('/css/estilo_landing_page.css') }}">
    <link rel="stylesheet" href="{{ url('/css/estilo_profile.css') }}">

    {{-- FavIcon --}}
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('/assets/favicon_io/fav.svg') }}">
    <link rel="icon" sizes="32x32" href="{{ url('/assets/favicon_io/fav.svg') }}">
    <link rel="icon" sizes="16x16" href="{{ url('/assets/favicon_io/fav.svg') }}">
    <link rel="manifest" href="{{ url('/assets/favicon_io/site.webmanifest') }}">

    {{-- Font Family --}}
    <link href="https://fonts.googleapis.com/css2?family=Quicksand&display=swap" rel="stylesheet">

    {{-- <title>Pinturillo | Home ?></title> --}}



    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" ></script>

  </head>
  <body>
    <div class="main">
        {{-- @extends('layouts.master') --}}
        {{-- MENU TODO: probar a centrarlo  --}}

          <nav class="navbar navbar-expand-lg navbar-dark ">
            <a class="navbar-brand"  href="{{ url('/home') }}">
                <img src="http://localhost/daw_proyectofinal/krylic/public/assets/imagenes/landing_page/logoBlancoXS.svg" width="50" height="30" class="d-inline-block align-top" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item ">
                    <a class="nav-link" href="{{ url('/home') }}">Home <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ url('/partidas') }}">Partidas</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link active" href="#">Perfil</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Chat</a>
                  </li>
              </ul>

              <form class="form-inline my-2 my-lg-0">
                <span class="glyphicon glyphicon-search"></span>
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
              </form>
            </div>
          </nav>

        {{-- PERFIL --}}
        <div class="container" >
            {{-- Info Usuiario--}}
            <div class="row info-profile" >
                <div class="col-md-3" >
                    <img class="rounded-circle mx-auto d-block border border-light" src="http://localhost/daw_proyectofinal/krylic/public/assets/imagenes/profile/profile1.jpg" alt="">
                </div>
                <div class="col-md-9" style="background: white">
                    <div class="row">
                        <div class="col-md-9">
                            <p class="h3"> {{ $usuarioRegistrado->name}}
                                {{-- <a href="" class="btn-karla">Editar Perfil</a> --}}
                                <button type="button" class="btn btn-outline-secondary">Editar Perfil</button>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <p> Nivel 3 </p>
                        </div>
                        <div class="col-md-2">
                            <p>5 Partidas</p>
                        </div>
                        <div class="col-md-2">
                            <p> 2 Ganadas</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            {{-- Redes Sociales Iconos --}}
                            <ul class="social-network social-circle">
                                <li><a href="#" class="icoInstagram" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="icoTwitch" title="Twitch"><i class="fa fa-twitch"></i></a></li>
                                {{-- TODO - No lo reconoce <li><a href="#" class="icoPatreon" title="Patreon"><i class="fa fa-patreon"></i></a></li> --}}
                            </ul>

                            {{-- @forelse ($dibujosUsuario as $dibujo)
                                <p> {{ $dibujo->imagen }} / {{ $dibujo->votos }} / {{ $dibujo->ronda_id }} / {{ $dibujo->fecha }} </p>
                            @empty
                                <p>No tiene nada</p>
                            @endforelse --}}
                        </div>
                    </div>
                </div>
                {{-- <h2>Usuario</h2>
                <button type="button" class="btn btn-light">Editar Perfil</button> --}}
                {{-- <div class="col-md-9">
                    <p class="h3">Usuario <button type="button" class="btn btn-outline-secondary">Editar Perfil</button> </p>
                    <p> 5 Partidas</p>
                </div> --}}
            </div>
            <hr>

            {{-- Galeria --}}
            <div class="row galeria-profile ">
                @forelse ($dibujosUsuario as $dibujo)
                    <figure class="col-md-4 dibujo">
                        <a href="">
                            <img src="{{ asset('assets/imagenes/'. $dibujo->imagen) }}" class="img-fluid"
                                alt="">
                        </a>
                    </figure>
                    {{-- <p> {{ $dibujo->imagen }} / {{ $dibujo->votos }} / {{ $dibujo->ronda_id }} / {{ $dibujo->fecha }} </p> --}}
                @empty
                    <p>No tiene nada</p>
                @endforelse
                {{-- <figure class="col-md-4 dibujo" >
                    <a href="">
                        <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(117).jpg" class="img-fluid" alt="">
                    </a>
                </figure>
                <figure class="col-md-4 dibujo" >
                    <a href="">
                        <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(126).jpg" class="img-fluid" alt="">
                    </a>
                </figure>
                <figure class="col-md-4 dibujo" >
                    <a href="">
                        <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(95).jpg" class="img-fluid" alt="">
                    </a>
                </figure>
                <figure class="col-md-4 dibujo" >
                    <a href="">
                        <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(120).jpg" class="img-fluid" alt="">
                    </a>
                </figure>

                <figure class="col-md-4 dibujo" >
                    <a href="">
                        <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(117).jpg" class="img-fluid" alt="">
                    </a>
                </figure>
                <figure class="col-md-4 dibujo" >
                    <a href="">
                        <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(126).jpg" class="img-fluid" alt="">
                    </a>
                </figure>
                <figure class="col-md-4 dibujo" >
                    <a href="">
                        <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(95).jpg" class="img-fluid" alt="">
                    </a>
                </figure>
                <figure class="col-md-4 dibujo" >
                    <a href="">
                        <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(120).jpg" class="img-fluid" alt="">
                    </a>
                </figure> --}}

            </div>
            {{-- <img src="http://localhost/daw_proyectofinal/krylic/public/assets/imagenes/img.webp" alt="..." class="img-thumbnail"> --}}
        </div>
        {{-- FOOTER --}}
        <footer>
            <div class="wave">   {{-- Efecto onda footer --}}
                    <div style="height: 150px; overflow: hidden;" >
                        <svg viewBox="0 0 500 150" preserveAspectRatio="none" style="height: 100%; width: 100%;">
                            <path d="M-21.72,41.94 C149.99,150.00 271.49,-49.98 516.64,63.64 L500.00,0.00 L-14.95,-16.28 Z" style="stroke: none; fill: #ffffff;"></path>
                        </svg>
                    </div>
            </div>
            <div class="page-footer fondo-degradado">
                <div class="container text-center text-md-left">
                    {{-- <div class="row info-footer">
                        <div class="col-md-6">
                            <h5 class="text-uppercase">Footer Content</h5>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat iste voluptatibus, qui </p>

                        </div>

                        <div class="col-md-3">
                            <h5 class="text-uppercase"> links </h5>
                            <ul class="list-unstyled">
                                <li> <a href="#"> Link 1 </a> </li>
                                <li> <a href="#"> Link 2 </a> </li>
                                <li> <a href="#"> Link 3 </a> </li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <h5 class="text-uppercase"> links </h5>
                            <ul class="list-unstyled">
                                <li> <a href="#"> Link 1 </a> </li>
                                <li> <a href="#"> Link 2 </a> </li>
                                <li> <a href="#"> Link 3 </a> </li>
                            </ul>
                        </div>
                </div> --}}
                <div class="footer-copyright text-center py-3 ">© {{ now()->year }} Copyright:
                    <a href="https://krylic.com/" class="btn" style="color:white"> krylic.com</a>
                </div>
            </div>

        </footer>
    </div>

    @section('scripts')
        <script type="text/javascript">
            const btn = document.getElementsByClassName('btn-karla');
            btn.addEventListener('click', function(e) {

                let x = e.clientX - e.target.offsetLeft;
                let y = e.clientY - e.target.offsetTop;

                let ripples = document.createElement('span');
                ripples.style.left = x + 'px';
                ripples.style.top = y + 'px';
                this.appendChild(ripples);
                console.log('hola');
            });
        </script>
    @endsection
</body>
</html>
