<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ url('/css/estilo_landing_page.css') }}">

    {{-- FavIcon --}}
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('/assets/favicon_io/fav.svg') }}">
    <link rel="icon" sizes="32x32" href="{{ url('/assets/favicon_io/fav.svg') }}">
    <link rel="icon" sizes="16x16" href="{{ url('/assets/favicon_io/fav.svg') }}">
    <link rel="manifest" href="{{ url('/assets/favicon_io/site.webmanifest') }}">

    <link rel="stylesheet" href="{{ url('/assets/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    {{-- Font Family --}}
    <link href="https://fonts.googleapis.com/css2?family=Quicksand&display=swap" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

    <title>Krylic | Home </title>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="main">
        {{-- HEADER--}}
        <header class="section-1">
            {{-- <a class="menu" href="" > Menu </a> --}}
            <div class="container">
                <div class="row mt-4 ">
                    <div class="w-100"></div>
                    <div class="col-lg-6 col-md-6 s-1-titulo">
                        <div style="height: 200p; padding-top: 100px">
                            <div class="logo"></div>
                        </div>
                        <h2> Ahora todos somos artistas </h2>
                        <div class="w-100 mt-4">
                            @if (Auth::user())
                                <a href="{{ route('home') }}" class="btn btn-outline-light btn-lg">Acceder</a>
                            @else
                                <a href="{{ url('/login') }}" class="btn btn-outline-light btn-lg">Acceder</a>
                                <a href="{{ url('/register') }}" class="btn btn-lg btn-borde" style="color:white">Registro</a>
                            @endif
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 pt-2 ilus-container">
                        <img class="img-fluid" src="{{ asset('assets/imagenes/landing_page/pintor-principal.svg') }}" class="ilus" alt="">
                    </div>
                </div>
            </div>
        </header>
        <div class="wave"> {{-- Efecto onda --}}
            <div style="height: 150px; overflow: hidden;">
                <svg viewBox="0 0 500 150" preserveAspectRatio="none" style="height: 100%; width: 100%;">
                    <path d="M0.00,49.98 C149.99,150.00 349.20,-49.98 500.00,49.98 L500.00,150.00 L0.00,150.00 Z"
                        style="stroke: none; fill: #fff;">
                    </path>
                </svg>
            </div>
        </div>

        {{-- BODY --}}
        {{-- TODO : CAMBIAR EL 1 DIV POR SECTION?¿ --}}
        {{-- <div class="section-2"> --}}
        <section class="my-auto section-2">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-5 text-center">
                        <img class="img-fluid" src="{{ asset('assets/imagenes/img-principal/img-principal-1.gif') }}" alt="">
                    </div>
                    <div class="col-md-7 ">
                        <h2>Concurso en líne para dibujar con tus amigos</h2>
                        <p> Si te gusta dibujar y eres competitivo o simplemente quieres pasar un buen rato con tus amigos,
                            ¡solo tienes que registrarte y dejar creer la imaginación!
                        </p>
                        <a href="{{ url('/login') }}" class="btn btn-degradado" >¡Registrate ya!</a>
                    </div>

                </div>
            </div>
        </section>

        <section class="my-auto section-3 text-white">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-7 ">
                        <h2>Tu progreso</h2>
                        <p>
                            Podrás ver siempre que quieras todas las ilustraciones que han participado en alguna partida.
                            ¡Solo tendrás que acceder a tu perfil y listo!
                        </p>
                        <a href="" class="btn btn-outline-light" >Empieza con tu primera partida</a>
                    </div>
                    <div class="col-md-5 text-center">
                        <img class="img-fluid" src="{{ asset('assets/imagenes/img-principal/img-principal-2.gif') }}" alt="">

                    </div>
                </div>
            </div>
        </section>

        <section class="my-auto section-4">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-5">
                        <img class="img-fluid" src=" {{ asset('assets/imagenes/img-principal/img-principal-3.gif') }}" alt="">
                    </div>
                    <div class="col-md-7">
                        <h2>Una familia</h2>
                        <p>
                            Krylic es más que una página de dibujos, es una cominudadCada día, miles de amigos se unen
                            con la idea de pasarlo bien, disfrutar dibujando y compartir sus obras.
                            ¡Dar riendas sueltas a ese Picasso que llevas dentro!
                        </p>
                        <a href="{{ url('/login') }}" class="btn btn-degradado" >Empieza</a>
                    </div>

                </div>
            </div>
        </section>
        {{-- </div> --}}

        {{-- Footer --}}
        <footer>
            <div class="wave"> {{-- Efecto onda footer --}}
                <div style="height: 150px; overflow: hidden;">
                    <svg viewBox="0 0 500 150" preserveAspectRatio="none" style="height: 100%; width: 100%;">
                        <path d="M-21.72,41.94 C149.99,150.00 271.49,-49.98 516.64,63.64 L500.00,0.00 L-14.95,-16.28 Z"
                            style="stroke: none; fill: #ffffff;"></path>
                    </svg>
                </div>
            </div>
            <div class="page-footer fondo-degradado">
                <div class="container text-center text-md-left">
                    <div class="row info-footer">
                        <div class="col-md-6">
                            <h5 class="text-uppercase" style="color: white">¿Quienes somos?</h5>
                            <p>
                                Somos una web que realiza concursos de dibujo de manera automatica.
                            </p>
                        </div>

                        <div class="col-md-3">
                            <h5 class="text-uppercase" style="color: white"> Twitter </h5>
                            <ul class="list-unstyled">
                                <li> <a href="https://twitter.com/KarlaNezCastae1"> <i class="fa fa-twitter-square" aria-hidden="true"></i> Twitter </a> </li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <h5 class="text-uppercase" style="color: white"> Instagram </h5>
                            <ul class="list-unstyled">
                                <li> <a href="https://www.instagram.com/krylic.web/"> <i class="fa fa-instagram" aria-hidden="true"></i> Instagram </a> </li>

                            </ul>
                        </div>

                    </div>
                    <div class="footer-copyright text-center py-3 ">© {{ now()->year }} Copyright:
                        <a href="http://krylic.es/" class="btn" style="color:white"> krylic.es</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</body>

</html>
