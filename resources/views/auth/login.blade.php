@extends('layouts.master')

@section('contenido')
<div class="row vh-100 vw-100 justify-content-center align-items-center auth-container">
    <div class="col-md-4" >
        <div class="auth-card auth-card-p">
            <div class="text-center">
                <a href="{{ url('/') }}">
                    <img src="{{ asset('assets/imagenes/landing_page/logoBlanco.svg')}}" height="100" class="logo-p d-inline-block text-center" alt="">
                </a>
            </div>

            <form method="POST" action="{{ route('login') }}" class="mt-5">
                @csrf
                <div class="col-md-8 d-flex flex-column justify-content-center offset-md-2">
                    <div class="row-md-12 p-2 mb-20">
                        <div class="wrapper">
                            <div class="input-data">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                <div class="underline"></div>
                                <label for="email">EMAIL</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 d-flex flex-column justify-content-center offset-md-2">
                    <div class="row-md-12 p-2 mb-20">
                        <div class="wrapper">
                            <div class="input-data">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                <div class="underline"></div>
                                <label for="nombre">CONTRASEÑA</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12 text-center">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label text-white" for="remember">
                                {{ __('Recordar usuario') }}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-degradado">
                            {{ __('Acceder') }}
                        </button>
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <div class="col-md-12 justify-content-center">
                        @if (Route::has('password.request'))
                                <a class="enlace-usuario" href="{{ route('password.request') }}">
                                    {{ __('¿Olvidaste tu contraseña?') }}
                                </a>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection