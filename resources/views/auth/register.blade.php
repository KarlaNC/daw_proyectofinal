@extends('layouts.master')

@section('contenido')
    <div class="row vh-100 vw-100 justify-content-center align-items-center auth-container pb-4">
        <div class="col-md-4 " style="margin-right: 50px">
            <div class="auth-card auth-card-p">
                <div class="text-center">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('assets/imagenes/landing_page/logoBlanco.svg')}}" height="100" class="logo-p d-inline-block text-center" alt="">
                    </a>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="col-md-10 d-flex flex-column justify-content-center offset-md-1">
                            <div class="row-md-12 pb-2 mb-20">
                                <div class="wrapper">
                                    <div class="input-data">
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  autocomplete="name" required>
                                        <div class="underline"></div>
                                        <label for="name">NOMBRE</label>
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-10 d-flex flex-column justify-content-center offset-md-1 ">
                            <div class="row-md-12 pb-2 mb-20">
                                <div class="wrapper">
                                    <div class="input-data">
                                        <input id="nick" type="text" class="form-control @error('nick') is-invalid @enderror" name="nick" value="{{ old('nick') }}" autocomplete="nick" required>
                                        <div class="underline"></div>
                                        <label for="nick">APODO/NICKNAME</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-10 d-flex flex-column justify-content-center offset-md-1 ">
                            <div class="row-md-12 pb-2 mb-20">
                                <div class="wrapper">
                                    <div class="input-data">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" required>
                                        <div class="underline"></div>
                                        <label for="email">EMAIL</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-10 d-flex flex-column justify-content-center offset-md-1">
                            <div class="row-md-12 pb-2 mb-20">
                                <div class="wrapper">
                                    <div class="input-data">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password" required>
                                        <div class="underline"></div>
                                        <label for="password">CONTRASEÑA</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-10 d-flex flex-column justify-content-center offset-md-1">
                            <div class="row-md-12 pb-2 mb-20">
                                <div class="wrapper">
                                    <div class="input-data">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password" required>
                                        <div class="underline"></div>
                                        <label for="password-confirm">CONFIRMAR CONTRASEÑA</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 justify-content-center text-center">
                            <button type="submit" class="btn btn-outline-light" >
                                {{ __('Regístrarte') }}
                            </button>
                        </div>
                    </form>
                    <a href="{{ url('/login') }}" class="enlace-usuario">¿Ya tienes una cuenta en Krylic? Inicia sesión</a>
                </div>
            </div>
        </div>
        <div class="col-md-4" style="margin-top: 100px;">
            <img src="{{ asset('assets/imagenes/auth/Dreamer.gif')}}" height="500"  alt="">
        </div>
    </div>
@endsection
