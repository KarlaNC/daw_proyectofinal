@extends('layouts.master')

@section('contenido')
    <div class="row vh-100 vw-100 justify-content-center align-items-center auth-container pb-4">
        <div class="col-md-4" style="margin-right: 50px">
            <div class="auth-card auth-card-p">
                <a href="{{ url('/') }}">
                    <img src="{{ asset('assets/imagenes/landing_page/logoBlanco.svg')}}" height="100" class="logo-p d-inline-block text-center ml-25" alt="">
                </a>
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <div class="col-md-9 d-flex flex-column justify-content-center offset-md-1">
                        <div class="row-md-12 p-2 mb-20">
                            <div class="wrapper">
                                <div class="input-data">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" required>
                                    <div class="underline"></div>
                                    <label for="email">EMAIL</label>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mb-0 justify-content-center align-items-center">
                        <div class="col-md-7">
                            <button type="submit" class="btn btn-outline-light">
                                {{ __('Restablecer contraseña') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4" >
            {{-- style="margin-top: 100px;" --}}
            <img src="{{ asset('assets/imagenes/auth/Forgot password-2.gif')}}" alt="">
        </div>
    </div>
@endsection
