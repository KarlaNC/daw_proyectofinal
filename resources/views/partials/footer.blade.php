@if($controller == 'perfil' || \Illuminate\Support\Facades\Request::segment(1) == 'home' || \Illuminate\Support\Facades\Request::segment(2) == 'crear')

<footer class="info-footer">
    {{-- Efecto onda footer --}}
    <div class="wave"> 
        <div style="height: 150px; overflow: hidden;">
            <svg viewBox="0 0 500 150" preserveAspectRatio="none" style="height: 100%; width: 100%;">
                <path d="M-21.72,41.94 C149.99,150.00 271.49,-49.98 516.64,63.64 L500.00,0.00 L-14.95,-16.28 Z"
                    style="stroke: none; fill: #ffffff;"></path>
            </svg>
        </div>
    </div>
    <div class="page-footer fondo-degradado">
        <div class="container text-center text-md-left">
               <div class="footer-copyright text-center py-3 ">© {{ now()->year }} Copyright:
                <a href="http://krylic.es/" class="btn" style="color:white"> krylic.es</a>
            </div>
        </div>
    </div>
</footer>
@endif
