<nav class="navbar navbar-expand-lg navbar-dark fondo-degradado">
    <a class="navbar-brand pl-3" href="{{ route('home') }}">
        <img src="{{ asset('assets/imagenes/landing_page/logoBlancoXS.svg')}}"
            width="50" height="30" class="logo-p d-inline-block align-top" alt="">
        </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item ">
                <a class="nav-link {{ \Illuminate\Support\Facades\Request::segment(1) == 'home' ? 'activo' : '' }}" 
                href="{{ route('home') }}"> Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ \Illuminate\Support\Facades\Request::segment(2) == 'privadas' ? 'activo' : '' }}" href="{{ route('partidas.listado') }}"> Mis Partidas </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ \Illuminate\Support\Facades\Request::segment(2) == 'publicas' ? 'activo' : '' }}" href="{{ route('partidas.publicas') }}"> Partidas publicas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ \Illuminate\Support\Facades\Request::segment(2) == 'invitaciones' ? 'activo' : '' }}" href="{{ url('/partidas/invitaciones') }}"> Invitaciones <span class="badge badge-light"> {{ \App\PartidaInvitacion::getNumInvitaciones() }} </span> </a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right pr-3">
            <li class="dropdown dropdown-menu-right">
                <div class="row">
                    <div class="col rounded-circle avatar-navbar" style="background-image: url({{ asset('storage/'.Auth::user()->avatar) }});">
                    </div>
                    <div class="col d-flex justify-content-center text-center">
                        <a class="dropdown-toggle estilo-drop" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        Hola, {{Auth::user()->name}}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="{{ url('/perfil') }}" ><i class="fa fa-user mr-2" aria-hidden="true"></i> Perfil</a>
                            <a class="dropdown-item" href="{{ url('/amigos') }}" ><i class="fa fa-user-plus mr-2" aria-hidden="true"></i> Amigos</a>
                            <a class="dropdown-item" href="{{ url('/partidas') }}" ><i class="fa fa-paint-brush mr-2" aria-hidden="true"></i> Mis partidas</a>
                            <a class="dropdown-item" href="{{ url('/logout') }}" ><i class="fa fa-sign-out mr-2" aria-hidden="true"></i> Desconectar </a>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</nav>
