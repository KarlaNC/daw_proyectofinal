@extends('layouts.master')

@section('titulo')
Index
@endsection

@section('contenido')

{{-- Compruebo si esta registrado --}}

@if (session('mensaje'))
<div class="alert alert-success" role="alert">
    {{ session('mensaje') }}
</div>
@endif

<div class="rondas-tema-bg">
    <div class="container">
        {{ Breadcrumbs::render('temas', $partida) }}
        <div class="row">
            <div class="col-md-6 offset-md-4" style="margin-top:100px">
                <h1>Tema de la partida</h1>
                <br>
                @if ($participantes_actuales >= 10)
                    <p>Lo sentimos, actualmente la partida esta completa!</p>
                @else
                    {{-- <h1>Partidas</h1> --}}
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:{{$temas_partida * 10 .'%;'}}">
                            <span class="sr-only">{{$porcentaje}}% completado</span>
                        </div>
                    </div>
                    <p>{{ $temas_partida }} / 10 participantes</p>
            
                    @if ($tema_usuario->exists() != 0)
                        <h1>Tú tema</h1>
                        <div class="form-group">
                            <label for="titulo">Titulo ronda</label>
                            <input type="text" class="form-control" value="{{ $tema_usuario->first()->titulo }}" readonly>
                        </div>
                        <div class="form-group">
                            <label for="descripcion">Descripción del dibujo</label>
                            <textarea class="form-control" rows="3" readonly>{{ $tema_usuario->first()->descripcion }}</textarea>
                        </div>
                    @else
                        <form action="{{ route('rondas.tema', $partida) }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="titulo">Titulo ronda</label>
                                <input type="text" class="form-control" name="titulo">
                            </div>
                            <div class="form-group">
                                <label for="descripcion">Descripción del dibujo </label>
                                <textarea class="form-control" name="descripcion" rows="3"></textarea>
                            </div>
            
                            <button class="btn btn-degradado">Enviar</button>
                        </form>
                    @endif
                @endif
            </div>
            {{-- <div class="col-md-2 mt-5 offset-md-1">
                <img class="prueba" src="{{ asset('assets/imagenes/rondas/temas/Design community (4).gif') }}" style="float:bottom">
            </div> --}}
        </div>
    </div>
</div>

@endsection
