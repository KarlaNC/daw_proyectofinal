@extends('layouts.master')

@section('titulo')
Index
@endsection

@section('contenido')

{{-- Compruebo si esta registrado --}}
@if (session('mensaje'))
<div class="alert alert-success" role="alert">
    {{ session('mensaje') }}
</div>
@endif

<div class="partidas-rondas-bg">
    <div class="container-fluid position-relative">
        <div class="texto-vertical"> Mis rondas </div>

        <div class="row mt-2">
            <div class="col-md-8 offset-md-1">
                {{ Breadcrumbs::render('rondas', $partida ) }}
                <div class="row">
                    @forelse ($arrayRondas as $ronda)
                    
                    <div class="col-md-3 rondaAvatar" data-nombre-ronda="{{ $ronda->slug }}">
                    @if ($ronda->estado_id == 3)
                        <div class="profile-card-2" onclick="document.location.href = '{{ route('rondas.dibujos', [$ronda->partida, $ronda]) }}'">
                    @elseif ($ronda->estado_id == 2)
                        <div class="profile-card-2" onclick="document.location.href = '{{ route('rondas.listado', $ronda->partida) }}'">
                    @elseif ($ronda->estado_id == 5)
                        <div class="profile-card-2" onclick="document.location.href = '{{ route('dibujos.ganadores', [$ronda->partida, $ronda]) }}'">
                    @else
                        <div class="profile-card-2" onclick="document.location.href = '{{ route('dibujos.listado', [$ronda->partida, $ronda]) }}'">
                    @endif
                            <img class="img img-responsive" >
                            <div class="profile-name"> {{$ronda->tema->titulo}}  </div>
                            <div class="profile-username"> {{$ronda->estado->estado}}</div>
                        </div>
                    </div>
                    @empty
                    <tr>
                        <td rowspan="3">Sin resultados</td>
                    </tr>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection