<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Amigos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{ action('PartidaController@postSelectAmigos') }}" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }} 
            @forelse ($arrayAmigos as $amigo)
            <div id="amigos" class="row align-items-center border-select">
                <div class="col-md-2" >
                    <div class="avatar-circulo avatar-select rounded-circle" style="background-image: url({{ asset('storage/'.$amigo->avatar) }});"> </div>
                </div>
                
                <div class="col-md-8">
                    <p>{{$amigo->nick}}</p>
                    <p>{{$amigo->email}}</p>
                </div>

                <div class="col-md-2">
                    {{-- <input type="checkbox" name="invitados[]" value="{{$amigo->id}}"> --}}
                    <label class="checkmark-container">Invitar
                      <input type="checkbox" name="invitados[]" value="{{$amigo->id}}">
                      <span class="checkmark"></span>
                    </label>
                </div>
            </div>
            @empty
                <tr>
                    <td rowspan="3">Sin resultados</td>
                </tr>
            @endforelse
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button id="amigosSelect" type="submit" class="btn btn-degradado" data-dismiss="modal">Enviar solicitudes</button>
        </div>
    </form>
    </div>
  </div>
</div>