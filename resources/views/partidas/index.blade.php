@extends('layouts.master')

@section('titulo')
Index
@endsection


@section('contenido')

@if (session('mensaje'))
    <div class="alert alert-success" role="alert">
        {{ session('mensaje') }}
    </div>
@endif
{{-- Listado de partidas --}}
<div class="container">
    {{-- {{ Breadcrumbs::render('home') }} --}}
    <div class="row">
        <div class="vertical-center" onclick="document.location.href = '{{ route('partidas.listado') }}'">
            <div class="inicio-accion">
                <div class="inicio-imagenes">
                    <label class="inicio-label" for="">MIS PARTIDAS</label>
                    <div class="capa-gris"></div>
                    <img class="img-cuadro" src="{{ asset('assets/imagenes/partidas/mis_partidas_cuadro.svg') }}">
                    <img class="img-artista" src="{{ asset('assets/imagenes/partidas/mis_partidas_artista.svg') }}">
                </div>
            </div>
        </div>
        <div class="vertical-center" onclick="document.location.href = '{{ route('partidas.crear') }}'">
            <div class="inicio-accion">
                <div class="inicio-imagenes">
                    <label class="inicio-label" for="">CREAR PARTIDA</label>
                    <div class="capa-gris"></div>
                    <img class="img-cuadro" src="{{ asset('assets/imagenes/partidas/crear_partida_lienzo.svg') }}">
                    <img class="img-artista" src="{{ asset('assets/imagenes/partidas/crear_partida_artista.svg') }}">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection