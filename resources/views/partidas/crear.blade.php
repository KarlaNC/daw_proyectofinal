@extends('layouts.master')

@section('titulo')
Crear
@endsection

@section('contenido')

@if (session('mensaje'))
<div class="alert alert-danger" role="alert">
    {{ session('mensaje') }}
</div>
@endif

@if (session('error'))
<div class="alert alert-danger" role="alert">
    {{ session('error') }}
</div>
@endif

@include('partidas.modal-amigos')

<div class="container">
    {{ Breadcrumbs::render('crearPartidas') }}
    <section class="align-items-center">
        <form action="{{ route('partidas.crear') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row justify-content-center align-items-center">
                <div class="col-md-6 justify-content-center">
                    <img class="prueba" src="{{ asset('assets/imagenes/partidas/Gaming.gif') }}" >
                    {{-- img-fluid --}}
                </div>
                <div class="col-md-4 d-flex flex-column justify-content-center offset-md-2 ">
                    <div class="row p-2 mb-20">
                        <div class="wrapper">
                            <div class="input-data">
                                <input type="text" name="nombre" required>
                                <div class="underline"></div>
                                <label for="nombre">NOMBRE PARTIDA</label>
                            </div>
                        </div>
                    </div>
                    <div class="row p-2 mb-20">
                        <div class="wrapper">
                            <div class="input-data">
                                {{-- <input class="select-fecha" type="text" name="fecha_inicio" required> --}}
                                <input type="text" id="fecha_inicio" name="fecha_inicio" required>
                                <div class="underline"></div>
                                <label for="nombre">FECHA INICIO</label>
                            </div>
                        </div>
                    </div>

                    <select name="tipoPartida" class="form-select" aria-label="Default select example">
                        <option selected value="publica">Publica</option>
                        <option value="privada">Privada</option>
                    </select>

                    <span class="badge badge-crear">Invita a tus amigos * Recuerda invitar minimo a 2 amigos *</span>
                    <div class="row span-show">
                        {{-- TODO CONTROLAR QUE SEAN MINIMO 3 invitados --}}
                        <img class="btn-plus img-fluid"  data-toggle="modal" data-target="#exampleModal" src="{{ asset('assets/imagenes/partidas/plus.png') }}">
                        <span class="badge spam-amigos" >Añadir amigos</span>
                        @if (isset($arrayAmigosSelect))
                            <input type="text" name="arrayAmigosSelect" value="{{ json_encode($arrayAmigosSelect)}}" hidden>
                        @endif
                    </div>
                </div>
            </div>
            <input type="hidden" name="amigosInvitados[]" id="amigosInvit">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-outline-light btn-degradado-reves" value="Serialize">Crear partida</button>
                </div>
            </div>
        </form>
    </section>
</div>
@endsection
