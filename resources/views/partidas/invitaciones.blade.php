@extends('layouts.master')

@section('titulo')
Index
@endsection

@section('contenido')

{{-- Compruebo si esta registrado --}}
@if (session('mensaje'))
<div class="alert alert-success" role="alert">
    {{ session('mensaje') }}
</div>
@endif

<div class="container">
    {{ Breadcrumbs::render('invitaciones') }}
    {{-- Boton de aceptar si aceptas te lleva a escribir los temas --}}
    <div class="row mt-3" >
        <h1 class="ml-4"> Invitaciones pendientes </h1>
        <div class="col-md-8  offset-md-1"> 
        <!-- offset-md-1 -->
            <div class="row">
                @foreach ($invitaciones as $invitacion)
                <div class="col-md-4 bloque-invitaciones"> 
                        <h5 class="mb-0">{{ $invitacion->partida->nombre_partida }}</h5>
                        <p class="text-uppercase text-muted">{{ $invitacion->partida->fecha_inicio }} // {{ $invitacion->estado->estado }}</p>
                        <button class="btn btn-degradado" onclick="document.location.href = '{{  route('rondas.tema', $invitacion->partida) }}'">Participar</button>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="row">
        <img class="prueba" src="{{ asset('assets/imagenes/partidas/invitaciones/Invite.gif') }}" style="left: 70%; bottom: 20% ;position:absolute">
    </div>
</div>

@endsection
