@extends('layouts.master')

@section('titulo')
Index
@endsection

@section('contenido')

{{-- Compruebo si esta registrado --}}
@if (session('mensaje'))
<div class="alert alert-success" role="alert">
    {{ session('mensaje') }}
</div>
@endif
{{-- Listado de partidas --}}
<div class="partidas-rondas-bg">
    <div class="container-fluid position-relative">
        <div class="texto-vertical t-240"> Partidas publicas</div>
        
        <div class="row mt-2">
            <div class="col-md-8 offset-md-1">
                {{ Breadcrumbs::render('partidasPublicas') }}
                <div class="row">
                    @forelse ($arrayPartidas as $partida)
                    <div class="col-md-3 partida" data-nombre-partida="{{ $partida->nombre_partida }}">
                    @if ($partida->estado_id == 2)
                        <div class="profile-card-2" onclick="document.location.href = '{{ route('rondas.tema', $partida) }}'">
                    @else
                        <div class="profile-card-2" onclick="document.location.href = '{{ route('rondas.listado', $partida) }}'">
                    @endif
                            <img class="img img-responsive b-f05">
                            <div class="profile-name t-sombra"> {{$partida->nombre_partida}} </div>
                            <div class="profile-username t-sombra"> {{$partida->estado->estado}} </div>
                        </div>
                    </div>
                    @empty
                    <tr>
                        <td rowspan="3">Sin resultados</td>
                    </tr>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
