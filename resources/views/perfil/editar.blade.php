@extends('layouts.master')

@section('titulo')
Index
@endsection

@if (session('mensaje'))
<div class="alert alert-success" role="alert">
    {{ session('mensaje') }}
</div>
@endif

@section('contenido')
<div class="container">
    <div class="row mt-5 cuadrado-x">
        <div class="col-md-4 cuadrado-izq">
            <ul>
                <li class="li-izq f-bold pointer" style="border-color: #f05a70" tabindex="0" onclick="window.location='{{ url('/perfil/editar') }}'">
                    Editar Perfil
                </li>
                <li class="li-izq pointer" tabindex="0" onclick="window.location='{{ url('/perfil/cambio') }}'">
                    Cambiar contraseña
                </li>
            </ul>
        </div>
        <div class="col-md-8 cuadraro-der">
            <form action="{{ action('PerfilController@postEditar') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <div class="row form-group">
                        <label for="avatar" class="col-sm-2 col-form-label pointer ">
                            <div class="avatar-circulo avatar-1 rounded-circle" style="background-image: url({{ asset('storage/'.Auth::user()->avatar) }});"></div>
                        </label>
                        <div class="col-sm-10 align-self-center">
                            {{-- <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile01">
                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                              </div> --}}
                            <input type="file" class="pointer " name="avatar" id="avatar">
                        </div>
                    </div>
                    <div class="row form-group align-items-center">
                        <label for="name" class="col-sm-2 col-form-label f-bold">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}">
                        </div>
                    </div>
                    <div class="row form-group align-items-center">
                        <label for="email" class="col-sm-2 col-form-label f-bold">Correo electrónico</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control " name="email" value="{{ Auth::user()->email }}">
                        </div>
                    </div>
                    <div class="row form-group align-items-center">
                        <label for="instagram" class="col-sm-2 col-form-label f-bold">Instagram</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control " name="instagram" value="{{ Auth::user()->instagram }}">
                        </div>
                    </div> 
                    <div class="row form-group align-items-center">
                        <label for="twitter" class="col-sm-2 col-form-label f-bold">Twitter</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control " name="twitter" value="{{ Auth::user()->twitter }}">
                        </div>
                    </div>
                    <div class="row form-group align-items-center">
                        <label for="twitch" class="col-sm-2 col-form-label f-bold">Twitch</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control " name="twitch" value="{{ Auth::user()->twitch }}">
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-sm-10 align-self-end">
                            <button type="submit" class="btn btn-outline-light btn-degradado" > Guardar cambios </button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>
@endsection
