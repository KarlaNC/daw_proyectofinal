@extends('layouts.master')

@section('titulo')
Index
@endsection

@section('contenido')

@if (session('mensaje'))
<div class="alert alert-success" role="alert">
    {{ session('mensaje') }}
</div>
@endif

{{-- PERFIL --}}
<div class="container">
    {{-- Info Usuiario--}}
    <div class="row p-30 info-profile ">
        {{-- IMG PERFIL --}}

        <div class="col-md-3 ">
            <div class="avatar-2 avatar-circulo rounded-circle d-inline-flex" style="background-image: url({{ asset('storage/'.Auth::user()->avatar) }});">
                <input type="file" class="pointer" name="avatar" id="avatar" hidden>
            </div>
        </div>
        <div class="col-md-9" style="background: white">
            <div class="row">
                <div class="col-md-9">
                    <p class="h3"> {{ $usuario->name}}
                        <a class="btn btn-outline-light btn-degradado mx-4" href="{{ url('/perfil/editar') }}">Editar
                            Perfil</a>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <p> Nivel {{$nivel}} </p>
                </div>
                <div class="col-md-2">
                    <p> Ganadas</p>
                </div>
                <div class="col-md-2">
                    <p>{{$usuario->numAmigos()}} Amigos</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    {{-- Redes Sociales Iconos --}}
                    <ul class="social-network social-circle">
                        <li><a href="{{ $usuario->instagram }}" class="icoInstagram" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="{{ $usuario->twitter }}" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="{{ $usuario->twitch }}" class="icoTwitch" title="Twitch"><i class="fa fa-twitch"></i></a></li>
                        {{-- TODO - No lo reconoce <li><a href="#" class="icoPatreon" title="Patreon"><i class="fa fa-patreon"></i></a></li> --}}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid" style="padding-bottom: 170px">
    <div class="grid">
        @foreach ($dibujosUsuario as $dibujo)
        <div class="grid-item zoom">
            <img src="{{ asset('storage/'. $dibujo->imagen) }}" style="max-width:600px">
        </div>
        @endforeach
    </div>
    @empty($dibujosUsuario)
        <div class="page-load-status">
            {{-- Loader --}}
            <div class="loader-ellips infinite-scroll-request">
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
            </div>
            {{-- Mensaje fin --}}
            <p class="infinite-scroll-last text-center">FIN</p>
            {{-- Mensaje error --}}
            <p class="infinite-scroll-error text-center">Se ha producido un error al cargar las imagenes, intentelo de nuevo</p>
        </div>
    @endempty
</div>

<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<script>
    var elem = document.querySelector('.grid');
    var msnry = new Masonry( elem, {
        itemSelector: '.grid-item',
        gutter: 10,
        fitWidth: true
    });
    var elem2 = document.querySelector('.grid');
    var infScroll = new InfiniteScroll( '.grid', {
        path: '?page=@{{#}}',
        append: '.grid-item',
        outlayer: msnry,
        history: false,
        status: '.page-load-status',
    });
</script>
@endsection
