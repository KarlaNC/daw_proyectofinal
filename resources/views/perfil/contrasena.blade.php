@extends('layouts.master')

@section('titulo')
Index
@endsection


@section('contenido')
@if (session('mensaje'))
    <div class="alert alert-success" role="alert">
        {{ session('mensaje') }}
    </div>
@endif

@if (count($errors) > 0)
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
            {{ $error }}
        @endforeach
    </div>
@endif

<div class="container">
    <div class="row mt-5 cuadrado-x">
        <div class="col-md-4 cuadrado-izq">
            <ul>
                <li class="li-izq pointer" tabindex="0" onclick="window.location='{{ url('/perfil/editar') }}'">
                    Editar Perfil
                </li>
                <li class="li-izq f-bold pointer" style="border-color: #f05a70" tabindex="0" onclick="window.location='{{ url('/perfil/cambio') }}'">
                    Cambiar contraseña
                </li>
            </ul>
        </div>
        {{-- <div class="col d-flex justify-content-center mt-5"> --}}
        <div class="col-md-8 cuadraro-der">
            <form action="{{ action('PerfilController@postCambioPass') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{-- <div class="form-group row justify-content-end"> --}}
                <div class="row form-group">
                    <label for="pass_actual " class="col-sm-3 col-form-label f-bold">Contraseña actual</label>
                    <div class="col-sm-9 ">
                        <input class="form-control" type="password" name="pass_actual" >
                    </div>
                </div>
                <div class="row form-group">
                    <label for="pass_nueva" class="col-sm-3 col-form-label f-bold">Contraseña nueva</label>
                    <div class="col-sm-9 ">
                        <input class="form-control" type="password" name="pass_nueva" >
                    </div>
                </div>
                <div class="row form-group">
                    <label for="pass_confirmada" class="col-sm-3 col-form-label f-bold">Confirmar contraseña nueva</label>
                    <div class="col-sm-9 pt-2">
                        <input class="form-control" type="password" name="pass_confirmada" >
                    </div>
                </div>
                <div class="row justify-content-end">
                    <div class="col-sm-9 align-self-end">
                        <button type="submit" class="btn btn-outline-light btn-degradado" > Cambiar contraseña </button>
                    </div>
                    <div class="col-sm-9 align-self-end pt-3">
                        <label for="" class="recuperar-pass">¿Olvidaste tu contraseña?</label>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
