@extends('layouts.master')

@section('titulo')
    Crear
@endsection

@section('contenido')
<div class="dibujos-subir-bg">
    <div class="container">
        <div class="row ">
            {{ Breadcrumbs::render('subirDibujos', $ronda) }}
            <div class="col-md-8 offset-md-4">
                <div class="card dibujo-input">
                    <form action="{{ route('dibujos.subir', [$ronda->partida, $ronda]) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                        <div class="card-body" style="padding:30px">
                            <input type="file" class="btn btn-degradado btn-block" name="dibujo" id="dibujo">
                        </div>
                        <div class="form-group text-center">
                        <button type="submit" class="btn btn-degradado" >Subir dibujo</button>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
