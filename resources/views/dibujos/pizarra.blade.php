@extends('layouts.master')

@section('titulo')
Index
@endsection

@section('contenido')

@if (session('mensaje'))
    <div class="alert alert-success" role="alert">
        {{ session('mensaje') }}
    </div>
@endif

<script src="{{ url('/js/dibujos/jquery.drawr.combined.js?v=2') }}"></script>
<style>
    /* body {
        margin:0px;
        -webkit-touch-callout: none;
        -webkit-text-size-adjust: none;
        -webkit-user-select: none;
    } */

    .canvas-style {
        width: 100%;
        /* height: 80%; */
    }
    #contenedor-canvas {
        border: 2px solid gray;
        /* margin: 40px; */
    }
</style>

<div class="pizarra-ronda-bg">
    <div class="container">
        <div class="row mt-2">
            {{ Breadcrumbs::render('pizarraDibujos', $ronda) }}
            <div id="contenedor-canvas" class="col-10 offset-md-1"> 
                <canvas class="krylic-canvas drawr-test3 canvas-style"></canvas>
            </div>
            <input type="file" id="file-picker" style="display:none;">
        </div>
    </div>
</div>

<script type="text/javascript">

    function init_canvas1(){
        $("#drawr-container .krylic-canvas").drawr({
            "enable_tranparency" : true,
            "canvas_width" : 800,
            "canvas_height" : 800,
            "clear_on_init" : false
        });
        $("#drawr-container .krylic-canvas").drawr("start");
    }
    init_canvas1();
    $("#drawr-container2 .krylic-canvas").drawr({ "enable_tranparency" : false, "color_mode" : "presets" });
    $("#contenedor-canvas .krylic-canvas").drawr({ "enable_tranparency" : false });

    $("#drawr-container2 .krylic-canvas,#contenedor-canvas .krylic-canvas").drawr("start");
    
    //add custom save button.
    var buttoncollection = $("#contenedor-canvas .krylic-canvas").drawr("button", {
        "icon":"mdi mdi-folder-open mdi-24px"
    }).on("touchstart mousedown",function(){
        //alert("demo of a custom button with your own functionality!");
        $("#file-picker").click();
    });
    var buttoncollection = $("#contenedor-canvas .krylic-canvas").drawr("button", {
        "icon":"mdi mdi-content-save mdi-24px"
    }).on("touchstart mousedown",function(){
        var imagedata = $("#contenedor-canvas .krylic-canvas").drawr("export","image/jpeg");
        var element = document.createElement('a');
        element.setAttribute('href', imagedata);// 'data:text/plain;charset=utf-8,' + encodeURIComponent("sillytext"));
        element.setAttribute('download', "krylic.jpg");
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    });
    $("#file-picker")[0].onchange = function(){
        var file = $("#file-picker")[0].files[0];
        if (!file.type.startsWith('image/')){ return }
        var reader = new FileReader();
        reader.onload = function(e) { 
            $("#contenedor-canvas .krylic-canvas").drawr("load",e.target.result);
        };
        reader.readAsDataURL(file);
    };

</script>
@endsection