@extends('layouts.master')

@section('contenido')

{{-- Compruebo si esta registrado --}}
@if (session('mensaje'))
<div class="alert alert-success" role="alert">
    {{ session('mensaje') }}
</div>
@endif

<div class="ganador-ronda-bg">
    <div class="container-fluid position-relative" style="padding-bottom: 175px">
        <div class="texto-vertical"> Mis rondas </div>

        <div class="row mt-2">
            <div class="col-md-8 offset-md-1">
                {{ Breadcrumbs::render('dibujosGanador', $ronda ) }}
                <div class="col-md-8">
                    <table id="resultsTable" class="table table-bordered table-striped table-dark">
                        <thead>
                            <tr>
                              <th scope="col">Usuario</th>
                              <th scope="col">Votos recibidos</th>
                              <th scope="col">Puntuaciones</th>
                            </tr>
                          </thead>
                          <tbody>
                              @foreach ( $dibujos as $dibujo)
                              <tr>
                                    <th scope="row"> {{ $dibujo->autor($dibujo->user_id)->nick }}
                                    @if ($dibujo->votos == $votosMax)
                                        <i class="fa fa-star" aria-hidden="true" style="color: yellow"></i>  
                                    @endif
                                    </th>
                                    <td>{{ $dibujo->votos}}</td>
                                    <td>{{ $dibujo->puntos}}</td>
                                </tr>
                              @endforeach
                          </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection