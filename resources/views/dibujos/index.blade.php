@extends('layouts.master')

@section('titulo')
Index
@endsection

@section('contenido')

@if (session('mensaje'))
    <div class="alert alert-success" role="alert">
        {{ session('mensaje') }}
    </div>
@endif
<div class="dibujos-bg">
    <div class="container">
        <div class="row mt-2">
            {{ Breadcrumbs::render('rondasDibujos', $ronda ) }}
            <div class="col-md-10 ">
                <div class="row" style="height: 400px">
                    <div class="col-md-12 p-5 justify-content-around d-flex flex-column">
                        <div class="card-body card bg-light ">
                            {{-- <div class="form-group">
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" disabled>{{$ronda->tema->descripcion}}</textarea>
                            </div> --}}
                            <p>{{$ronda->tema->descripcion}}</p>
                        </div>
                        <div class="mt-5">
                            <div class="col-md-5 btn btn-dibujo" onclick="document.location.href = '{{ route('dibujos.pizarra', [$ronda->partida, $ronda]) }}'">
                                <img class="" src="{{ asset('assets/imagenes/partidas/dibujos/brush.png') }}">
                                ¡Dibuja!
                            </div>
                            <div class="col-md-5 btn btn-upload" onclick="document.location.href = '{{ route('dibujos.subir', [$ronda->partida, $ronda]) }}'">
                                <img class="img-btn-dibujo" src="{{ asset('assets/imagenes/partidas/dibujos/upload.png') }}">
                                ¡Sube tu dibujo!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
