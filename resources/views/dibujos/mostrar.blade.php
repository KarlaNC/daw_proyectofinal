@extends('layouts.master')

@section('titulo')
Mostrar
@endsection

@section('contenido')

<div class="row ml-3">
    <div class="card ">
        <img src="{{ asset('assets/imagenes/')}}/{{ $dibujoSeleccionado->imagen }}" style="height:500px; object-fit: cover;"
            class=" border border-light" />
    </div>
    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">Autor: </h5>
            <h6 class="card-subtitle mb-2 text-muted">{{ $dibujoSeleccionado->autor->name }}</h6>
            <p class="card-text"> Votos : {{ $dibujoSeleccionado->votos }}</p>
            <a href="#" class="card-link">Perfil usuario</a>
            <a href="{{ url('rondas/ver')}}/{{ $dibujoSeleccionado->ronda_id }}" type="button" class="card-link">
                Volver</a>
        </div>
    </div>
</div>

@endsection
