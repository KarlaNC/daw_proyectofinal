@extends('layouts.master')

@section('titulo')
Index
@endsection

@section('contenido')

@if (session('mensaje'))
    <div class="alert alert-success" role="alert">
        {{ session('mensaje') }}
    </div>
@endif

{{-- SI ESTA ACTIVA LA RONDA --}}

{{-- SI NO ESTA ACTIVA LA RONDA --}}

{{--
TODO :
    - 1. Mostrar dibujos
    2. Votos - solo uno por usuario de la partida (Partida Acabada)
    3. Subir dibujos
--}}

    <div class="container">
        <div class="texto-vertical t-240"> Dibujos </div>
        <div class="row mt-2">
            {{ Breadcrumbs::render('dibujosListado', $ronda ) }}
            <div class="col-md-10 ">
                <div class="row">
                    @foreach( $arrayDibujos as $dibujo )
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <div>
                                <div class="btn-group-vertical">
                                    <img src="{{ asset('storage/'. $dibujo->imagen) }}" class=" img-thumbnail" style="height:300px; object-fit: cover;">
                                </div>
                            </div>
                            @if ($anonimo  == 0 ||$voto_usuario->dibujo_voto == 0 && $ronda->estado_id == 4 || $voto_usuario->dibujo_voto == NULL && $ronda->estado_id == 4)
                                @if ($dibujo->user_id != $usuario_id)
                                    <form action="{{ route('dibujos.voto', [$ronda->partida, $ronda, $dibujo]) }}" method="post">
                                        {{ csrf_field() }}
                                        <input name="dibujoVoto" type="hidden" value="{{ $dibujo->id }}">
                                        <input type="submit" class=" ml-5 mt-2 btn btn-secondary btn-sm" value="Votar +1"/>
                                    </form>
                                @endif
                            @endif
                    
                            @if ($ronda->estado_id == 5 && $dibujo->user_id == $ganador->user_id)
                                <p> Es el ganador</p>
                            @endif
                
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
