<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partidas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_partida', 50);
            $table->string('slug');
            $table->string('codigo', 4)->nullable();
            $table->boolean('privada');

            
            $table->unsignedBigInteger('creado_por');
            $table->foreign('creado_por')->references('id')->on('users');
            
            $table->unsignedBigInteger('estado_id');
            $table->foreign('estado_id')->references('id')->on('estados_partidas')->onDelete('cascade');
           
            $table->date('fecha_inicio');
            $table->date('fecha_fin')->nullable();
            $table->integer('num_participantes');
            $table->time('duracion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partidas');
    }
}
