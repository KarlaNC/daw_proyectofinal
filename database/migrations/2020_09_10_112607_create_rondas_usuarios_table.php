<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRondasUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rondas_usuarios', function (Blueprint $table) {
            $table->primary(['ronda_id', 'user_id']);
            $table->unsignedBigInteger('ronda_id');
            $table->foreign('ronda_id')->references('id')->on('rondas')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('dibujo_voto')->nullable();
            $table->foreign('dibujo_voto')->references('id')->on('dibujos')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('ronda_usuario_puntos')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rondas_usuarios');
    }
}
