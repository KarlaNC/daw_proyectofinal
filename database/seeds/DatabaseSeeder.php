<?php

use App\User;
use App\Tema;
use App\Ronda;
use App\Dibujo;
use App\Partida;
use App\Puntuacion;
use App\EstadoRonda;
use App\GanadorPlus;
use App\EstadoPartida;
use Carbon\CarbonImmutable;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    private $arrayEstadoPartida = array(
        // Reorganizar estados
        array(
            'estado' => 'Activa'
        ),
        array(
            'estado' => 'Pendiente'
        ),
        array(
            'estado' => 'Completada'
        ),
        array(
            'estado' => 'Cancelada'
        ),

    );

    private $arrayEstadoRonda = array(
        // Reorganizar estados
        array(
            'estado' => 'Activa'
        ),
        array(
            'estado' => 'Pendiente'
        ),
        array(
            'estado' => 'Dibujando'
        ),
        array(
            'estado' => 'Votando'
        ),
        array(
            'estado' => 'Completada'
        ),
        array(
            'estado' => 'Cancelada'
        ),

    );

    private $arrayUsuarios = array(
        array(
            'name' => "Karla Núñez",
            'nick' => "Inori",
            'email' => "karla@gmail.com",
            'password' => 'karla123',
        ),
        array(
            'name' => "Sergio",
            'nick' => "FunkyPunky",
            'email' => "sergio@gmail.com",
            'password' => 'sergio123',
        ),
        array(
            'name' => "Lucia",
            'nick' => "Lucy",
            'email' => "lucia@gmail.com",
            'password' => 'lucia123',
        ),
        array(
            'name' => "Dani",
            'nick' => "Kratos",
            'email' => "dani@gmail.com",
            'password' => 'dani123',
        ),
        array(
            'name' => "Paula",
            'nick' => "Paula",
            'email' => "paula@gmail.com",
            'password' => 'paula123',
        ),
        array(
            'name' => "Alvaro",
            'nick' => "Keynnan",
            'email' => "alvaro@gmail.com",
            'password' => 'alvaro123',
        ),
        array(
            'name' => "Maria",
            'nick' => "Meri",
            'email' => "maria@gmail.com",
            'password' => 'maria123',
        ),
    );

    private $arrayGanadorPlus = array(
        array(
            'num_participantes' => 3,
            'plus' => 3,
            'dificultad' => 'Baja'
        ),
        array(
            'num_participantes' => 4,
            'plus' => 3,
            'dificultad' => 'Baja'
        ),
        array(
            'num_participantes' => 5,
            'plus' => 7,
            'dificultad' => 'Media'
        ),
        array(
            'num_participantes' => 6,
            'plus' => 7,
            'dificultad' => 'Media'
        ),
        array(
            'num_participantes' => 7,
            'plus' => 7,
            'dificultad' => 'Media'
        ),
        array(
            'num_participantes' => 8,
            'plus' => 15,
            'dificultad' => 'Alta'
        ),
        array(
            'num_participantes' => 9,
            'plus' => 15,
            'dificultad' => 'Alta'
        ),
        array(
            'num_participantes' => 10,
            'plus' => 15,
            'dificultad' => 'Alta'
        ),
    );

    private $arrayPuntuacion = array(
        array(
            'puntos' => 0,
            'estado' => 'No presentado'
        ),
        array(
            'puntos' => 1,
            'estado' => 'Participanción'
        ),
        array(
            'puntos' => 2,
            'estado' => 'Empate'
        ),
        array(
            'puntos' => 3,
            'estado' => 'Unico ganador'
        )
    );

    private $arrayPartidas = array(
        array(
            'nombre_partida' => 'Partida Seed',
            'codigo' => 'o1el',
            'privada' => 1,
            'creado_por' => 1,
            'estado_id' => 2,
            'fecha_inicio' => '2021-03-16',
            // 'fecha_fin' => 'No presentado',
            'num_participantes' => 0,
            'duracion' => '00:00:00'
        ) ,
        array(
            'nombre_partida' => 'LALALALALALALALA',
            'codigo' => 'o1el',
            'privada' => 0,
            'creado_por' => 1,
            'estado_id' => 2,
            'fecha_inicio' => '2021-03-16',
            // 'fecha_fin' => 'No presentado',
            'num_participantes' => 0,
            'duracion' => '00:00:00'
        )
        // ,
        // array(
        //     'nombre_partida' => 'PArtida 34',
        //     'codigo' => 'o1el',
        //     'creado_por' => 1,
        //     'estado_id' => 2,
        //     'fecha_inicio' => '2021-03-16',
        //     // 'fecha_fin' => 'No presentado',
        //     'num_participantes' => 0,
        //     'duracion' => '00:00:00'
        // ),
        // array(
        //     'nombre_partida' => 'asdasdasdasdA',
        //     'codigo' => 'o1el',
        //     'creado_por' => 1,
        //     'estado_id' => 2,
        //     'fecha_inicio' => '2021-03-16',
        //     // 'fecha_fin' => 'No presentado',
        //     'num_participantes' => 0,
        //     'duracion' => '00:00:00'
        // )
    );

    private $arrayTemas = array(
        array(
            'user_id' => 1,
            'partida_id' => 1,
            'titulo' => 'Ronda Karla',
            'descripcion' => 'Ronda Karla desc'
        ),
        array(
            'user_id' => 2,
            'partida_id' => 1,
            'titulo' => 'Ronda Sergio',
            'descripcion' => 'Ronda Sergio desc'
        ),
        array(
            'user_id' => 3,
            'partida_id' => 1,
            'titulo' => 'Ronda Lucia',
            'descripcion' => 'Ronda Lucia desc'
        ) , array(
            'user_id' => 1,
            'partida_id' => 2,
            'titulo' => 'Ronda Karla',
            'descripcion' => 'Ronda Karla desc'
        ),
        array(
            'user_id' => 2,
            'partida_id' => 2,
            'titulo' => 'Ronda Sergio',
            'descripcion' => 'Ronda Sergio desc'
        ),
        array(
            'user_id' => 3,
            'partida_id' => 2,
            'titulo' => 'Ronda Lucia',
            'descripcion' => 'Ronda Lucia desc'
        )
        // ,
        // array(
        //     'user_id' => 1,
        //     'partida_id' => 3,
        //     'titulo' => 'Ronda Karla',
        //     'descripcion' => 'Ronda Karla desc'
        // ),
        // array(
        //     'user_id' => 2,
        //     'partida_id' => 3,
        //     'titulo' => 'Ronda Sergio',
        //     'descripcion' => 'Ronda Sergio desc'
        // ),
        // array(
        //     'user_id' => 3,
        //     'partida_id' => 3,
        //     'titulo' => 'Ronda Lucia',
        //     'descripcion' => 'Ronda Lucia desc'
        // ),
        // array(
        //     'user_id' => 1,
        //     'partida_id' => 4,
        //     'titulo' => 'Ronda Karla',
        //     'descripcion' => 'Ronda Karla desc'
        // ),
        // array(
        //     'user_id' => 2,
        //     'partida_id' => 4,
        //     'titulo' => 'Ronda Sergio',
        //     'descripcion' => 'Ronda Sergio desc'
        // ),
        // array(
        //     'user_id' => 3,
        //     'partida_id' => 4,
        //     'titulo' => 'Ronda Lucia',
        //     'descripcion' => 'Ronda Lucia desc'
        // ),
        // array(
        //     'user_id' => 1,
        //     'partida_id' => 5,
        //     'titulo' => 'Ronda Karla',
        //     'descripcion' => 'Ronda Karla desc'
        // ),
        // array(
        //     'user_id' => 2,
        //     'partida_id' => 5,
        //     'titulo' => 'Ronda Sergio',
        //     'descripcion' => 'Ronda Sergio desc'
        // ),
        // array(
        //     'user_id' => 3,
        //     'partida_id' => 5,
        //     'titulo' => 'Ronda Lucia',
        //     'descripcion' => 'Ronda Lucia desc'
        // )
    );

    private $arrayRondas = array(
        'partida_id' => 2,
        'tema_id' => 4,
        'estado_id' => '',
        'fecha_inicio' => '',
        'fecha_votos' => '',
        'fecha_fin' => ''
    );
    // id	partida_id	tema_id	estado_id	fecha_inicio	fecha_votos	fecha_fin
    // rondas usuarios
    public function run()
    {
        // $this->call(UserSeeder::class);
        // $this->call(UsersTableSeeder::class);
        self::seedUser();
        self::seedEstadoPartida();
        self::seedEstadoRonda();
        self::seedGanadorPlus();
        self::seedPuntuacion();
        // self::seedPartida();
        // self::seedTemas();
        
        // self::seedPartida();
        // self::seedRonda();
        // self::seedDibujo();
        //$this->command->info('Tablas inicializadas con datos');
    }

    private function seedUser()
    {
        DB::table('users')->delete(); //Borrado de toda la tabla

        foreach ($this->arrayUsuarios as $user) {
            $u = new User(); //Dibujo::findOrFail($id);
            $u->name =  $user['name'];
            $u->nick =  $user['nick'];
            $u->slug =  Str::slug($user['nick']);
            $u->email = $user['email'];
            $u->password = bcrypt($user['password']);
            $u->save();
        }
    }

    private function seedPartida()
    {
        DB::table('partidas')->delete(); //Borrado de toda la tabla
        foreach ($this->arrayPartidas as $p) {
            $partida = new Partida();
            $partida->nombre_partida = $p['nombre_partida'];
            $partida->slug = Str::slug($p['nombre_partida']);
            $partida->codigo = $p['codigo'];
            $partida->privada =  $p['privada'];
            $partida->creado_por = 1;
            $partida->estado_id = $p['estado_id'];
            $partida->fecha_inicio = CarbonImmutable::now();
            $partida->num_participantes = 3; // TODO: ¿Lo dejo así?
            $partida->duracion = $p['duracion'];
            $partida->save();
        }
    }

    private function seedTemas()
    {
        DB::table('temas')->delete(); //Borrado de toda la tabla
        foreach ($this->arrayTemas as $t) {
            $temas = new Tema();
            $temas->user_id = $t['user_id'];
            $temas->partida_id = $t['partida_id'];
            $temas->titulo = $t['titulo'];
            $temas->descripcion = $t['descripcion'];
            $temas->save();
        }
    }

    private function seedRonda()
    {
        DB::table('rondas')->delete(); //Borrado de toda la tabla

        $ronda = new Ronda();
        $ronda->partida_id = Partida::all()->first()->id;
        $ronda->tema_ronda = "Elfos";
        $ronda->save();
    }

    private function seedDibujo()
    {
        DB::table('dibujos')->delete();

        foreach ($this->arrayDibujos as $dibujo) {
            $d = new Dibujo(); //Dibujo::findOrFail($id);
            $d->user_id =  $dibujo['user_id'];
            $d->ronda_id = $dibujo['ronda_id'];
            $d->fecha = $dibujo['fecha'];
            $d->votos = $dibujo['votos'];
            $d->imagen = $dibujo['imagen'];
            $d->save();
        }
    }

    private function seedEstadoPartida() {
        DB::table('estados_partidas')->delete();

        foreach ($this->arrayEstadoPartida as $estado) {
            $e = new EstadoPartida();
            $e->estado = $estado['estado'];
            $e->save();
        }
    }

    private function seedEstadoRonda() {
        DB::table('estados_rondas')->delete();

        foreach ($this->arrayEstadoRonda as $estado) {
            $e = new EstadoRonda();
            $e->estado = $estado['estado'];
            $e->save();
        }
    }

    private function seedGanadorPlus() {
        DB::table('ganador_plus')->delete();

        foreach ($this->arrayGanadorPlus as $ganador) {
            $ganador_plus = new GanadorPlus();
            $ganador_plus->num_participantes = $ganador['num_participantes'];
            $ganador_plus->plus = $ganador['plus'];
            $ganador_plus->dificultad = $ganador['dificultad'];
            $ganador_plus->save();
        }
    }

    private function seedPuntuacion() {
        DB::table('puntuaciones')->delete();

        foreach ($this->arrayPuntuacion as $puntuacion) {
            $p = new Puntuacion();
            $p->puntos = $puntuacion['puntos'];
            $p->estado = $puntuacion['estado'];
            $p->save();
        }
    }
}
