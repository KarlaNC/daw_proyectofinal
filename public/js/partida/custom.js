window.onload = function() {
    // CREAR PARTIDA
    $(".btn-plus").mouseenter(function() {
        $( ".spam-amigos" )
        .animate({
            width: "50%",
          }, {
            queue: false,
            duration: 1200
        })
        .animate({ 'text-align': "end" }, 0 );
        $(".btn-plus").css({
            "background-color": "#ffffff"
        });
    });


    $(".btn-plus").mouseout(function() {
        $( ".spam-amigos" ).animate({
            width: "2%",
          }, {
            queue: false,
            duration: 1200
        })
        .animate({ 'text-align': "end" }, 0 );

        $('.btn-plus')
        .delay(1200)
        .queue(function (next) { 
            $(this).css("background-color", "#f4899c"); 
            next(); 
        });
    });

    $('.btn-mandar').click(function(){
        if ($(this).hasClass('btn-degradado-reves')) {
            $(this).removeClass('btn-degradado-reves')
            .addClass('btn-secondary');
        } else {
            $(this).removeClass('btn-secondary')
            .addClass('btn-degradado-reves');
        }
    });

    // FECHA PLACEHOLDER OCULTO
    $('#fecha_inicio').focus(function() {
        $(this).prop('type', 'date');
    });

    $('#fecha_inicio').blur(function() {
        if($(this).val() == "") {
            $(this).prop('type', 'text');
        }
    });

    // BUSCAR AMIGOS
    $('#busqueda').keyup(function() {
        var busqueda = $('#formBusqueda').serialize();

        $.ajax({
            type: 'POST',
            url: url_global + "/partidas/buscarAmigos",
            data: busqueda,
            success:function(resultados) {
                var resultados = JSON.parse(resultados);
                resultados.forEach(resultado => {
                    // $('#amigos').html = "";
                    console.log(resultado.name);
                });
            }
         });
    });

    // AÑADIR AMIGOS
    $('#amigosSelect').click(function() {
        var arrayInvitaciones = [];
        $("[name='invitados[]']:checked").each(function() {
            arrayInvitaciones.push($(this).val());
            console.log(arrayInvitaciones);
        });
        $("[name='amigosInvitados[]']").val(arrayInvitaciones);
    });

    // CARGAR IMAGENES PARTIDAS
    var partidas = document.querySelectorAll('.partida');
    partidas.forEach(function(partida) {
        var nombre_partida = partida.dataset.nombrePartida;
        // Llamada a la api por GET
        partida.querySelector('img').src = "https://avatars.dicebear.com/api/identicon/" + nombre_partida + ".svg";
    });
}