document.addEventListener("DOMContentLoaded", function(event) {
    document.getElementById('buscar_amigos').focus();
    document.getElementById('buscar_amigos').addEventListener('keydown', buscarAmigos);
});

// var escribirNombreAmigo = function(event) {
//     document.getElementById('buscar_amigos').removeEventListener('keydown', escribirNombreAmigo);

//     var nombre_1;
//     var nombre_2;

//     setTimeout(function() {
//         nombre_1 = document.getElementById('buscar_amigos').value;
        
//         if(nombre_1 != "") {
//             buscarAmigos();
//         }
//         document.getElementById('buscar_amigos').addEventListener('keyup', escribirNombreAmigo);
//     }, 600);

//     setTimeout(function() {
//         nombre_2 = document.getElementById('buscar_amigos').value;
//         if (nombre_1 != nombre_2) {
//             buscarAmigos();
//         } else {
//             document.getElementById('amigos_encontrados').innerHTML = "";
//         }
//     }, 1200);
// }

var buscarAmigos = function() {
    var buscador_val = document.getElementById('buscar_amigos').value;
    var token = document.getElementById('token').value;
    var urlBuscarAmigo = document.getElementById('urlBuscarAmigo').value;

    var datos = {nombre: buscador_val, _token: token};

    if (buscador_val.trim() != "") {
        fetch(urlBuscarAmigo, {
            method: 'POST',
            body: JSON.stringify(datos),
            async: false,
            headers:{
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': token
            },
        })
        .then(function(response) {
            return response.text();
        })
        .then(function(response_content) {
            document.getElementById('amigos_encontrados').innerHTML = response_content;
        })
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response));
    }
}
