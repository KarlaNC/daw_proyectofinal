window.onload = function() {
    // CARGAR IMAGENES PARTIDAS
    var rondas = document.querySelectorAll('.rondaAvatar');
    rondas.forEach(function(ronda) {
        var nobmre_ronda = ronda.dataset.nombreRonda;
        // Llamada a la api por GET
        ronda.querySelector('img').src = "https://avatars.dicebear.com/api/identicon/" + nobmre_ronda + ".svg";
    });
}