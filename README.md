![Krylic](logo.png)

### Comenzando 🚀
Comenzaremos con clonar el repositorio, para ello ejecutaremos el siguiente comando (HTTP):  
```$ git clone git@gitlab.com:KarlaNC/daw_proyectofinal.git```

1.  Composer  
    - Nos posicionaremos en el proyecto, en mi caso C:\xampp\htdocs\krylic y ejecutaremos el comando  
    ```$ composer install```
    - En caso de error ejecutaremos:  
    ```$ composer self-update```  
    ```$ composer update```

2. Base de datos
    - Crearemos la BBDD en mi caso se llamará krylic y tiene que llevar la codificación utf8_unicode_ci
    - En la carpeta del proyecto deberemos cambiar de nombre el archivo **.env.example** por **.env**
    - En este mismo archivo añadiremos el nombre de la bbdd que anteriormente se ha creado  
    ```DB_DATABASE=krylic```
    - Hay que comprobar que en /config/database.php la codificación es **utf8_unicode_ci**  
    - Para migrar las tablas de base de datos que tiene el proyecto en  **database/migrations** ejecutaremos el siguiente comando  
    ``` $ php artisan migrate```

- ```$ php artisan:key generate```
- Ya tenemos acceso pero primero vamos a añadir datos a la aplicación mediante el comando ```$ php artisan db:seed```

3. Storage
    - Para que funcione el almacenamiento de imágenes deberemos añadir al fichero .env  
    ```FILESYSTEM_DRIVER=public```
    - Deberemos crear un link simbolico mediante el comando  
    ```$ php artisan storage:link```  
Esta paso es importante ya que sin el, no sé podrá subir ninguna imagen a la aplicación 
4. Procesos automáticos/Kernelvale

##  Pre-requisitos 📋
- Xampp instalado (Apache y Mysql)  

##  Instalación 🔧
1.  Composer :
    - Acceder a https://getcomposer.org/download/, descargar el .exe y ejecutarlo.  

##  Construido con 🛠️
- Laravel
- JQuery / JS Vanilla
- CSS / Bootstrap
- HTML