<?php

namespace App\Http\Controllers;

use App\User;
use App\Ronda;
use App\Dibujo;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PerfilController extends Controller
{
    public function perfil(Request $request)
	{
        $usuario = Auth::user();
        $user_id = $usuario->id;
        $dibujos_usuario = Dibujo::where('user_id', $user_id)->paginate(10);
        // if(empty($dibujos_usuario)) {
        $puntos = User::getPuntos($user_id);
        $nivel = User::getLvL($puntos);
        
        // Si no se solicita la página y esta no es la primera, podra responder 204
        if (!is_null($request->page) && $request->page != 1) {
            abort_if($dibujos_usuario->isEmpty(), 204);
        }

        return view('perfil.index', array('usuario' => $usuario, 'dibujosUsuario' => $dibujos_usuario, 'puntos' => $puntos, 'nivel' => $nivel));
    }

    public function getEditar()
    {
        $usuario = Auth::user();
        $user_id = $usuario->id;
        $dibujos_usuario = Dibujo::all()->where('user_id', $user_id);

        return view('perfil.editar', array('usuario' => $usuario, 'dibujosUsuario' => $dibujos_usuario));
    }

    public function postEditar(Request $request)
    {
        $usuario = Auth::user();

        try {

            if ($request->hasFile('avatar')) {
                $imagen = $request->file('avatar');
                $imagen_nombre = time() . '_' . $imagen->getClientOriginalName();
                $imagen_path = $request->file('avatar')->store('public');

                $usuario->avatar = $request->file('avatar')->storeAs('public', $imagen_nombre);
            }

            $usuario->name = $request->name;
            $usuario->email = $request->email;
            $usuario->twitter = $request->twitter;
            $usuario->instagram = $request->instagram;
            $usuario->twitch = $request->twitch;
            $usuario->save();

            return redirect("perfil")->with("mensaje", "Usuario actualizado");
		} catch (Exception $ex) { // \Illuminate\Database\QueryException
			return redirect("perfil")->with("mensaje", "Error al actualiza el usuario");
		}
    }

    public function getCambioPass()
    {
        return view('perfil.contrasena');
    }

    public function postCambioPass(Request $request)
    {
        $reglas = [
            'pass_actual'=> ['required'] ,
            'pass_nueva'=> ['required','min:6','max:18'],
        ];

        $mensajes = [
            'pass_actual.required' => 'El campo es requerido',
            'pass_nueva.required'=> 'El campo es requerido',
            'pass_nueva.min'=> 'El mínimo permitido son 6 carácteres',
            'pass_nueva.max'=> 'El máximo permitido son 18 carácteres'
        ];

        $usuario = Auth::user();
        $validator = Validator::make($request->all(), $reglas, $mensajes);

        if ($validator->fails()) {
            return redirect('perfil/cambio')->withErrors($validator->errors($mensajes));
        } else {
           if (Hash::check($request->pass_actual, $usuario->password)) {
                $user = new User();
                $user
                    ->where('email', '=', $usuario->email)
                    ->update(['password'=> bcrypt($request->pass_nueva)]);

                return redirect('perfil')->with("mensaje", "Contraseña cambiada con éxito");
           } else {
                return redirect('perfil/cambio')->with("mensaje","Error al actualizar");
           }
        }
    }
}
