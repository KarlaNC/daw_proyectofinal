<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Dibujo;
use App\Ronda;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class LandingPageController extends Controller
{
    // Landing Page
	public function home()
	{
		return view('layouts.landing_page');
    }
}
