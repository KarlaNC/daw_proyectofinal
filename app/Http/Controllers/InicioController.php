<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InicioController extends Controller
{
    public function getInicio(){
    	// return return view('home');
    	// TODO: Página por defecto. Se muestra sin estar logeado
        return redirect()->action('PartidaController@getPartidas');
        // Inicio - Dashboard
    }
}
