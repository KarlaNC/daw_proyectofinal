<?php

namespace App\Http\Controllers;

use App\User;
use App\Tema;
use App\Ronda;
use App\Partida;
use App\Models\RondaUsuario;
use App\PartidaInvitacion;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class RondasController extends Controller
{
    public function getRondas(Partida $partida)
    {
        $rondas = Ronda::all()->where('partida_id', $partida->id);
        $rondas_creadas = $rondas->count();

        if ($rondas_creadas == 0) {
            // Si no hay ninguna ronda creada redirecciona a la funcion getCrearRondas
            return $this->getCrearRondas($partida->id);
        } else {
            return view('rondas.index',  array('arrayRondas' => $rondas, 'partida' => $partida));
        }
    }

    public function getCrearRondas(Partida $partida)
    {
        $temas_partida = Tema::where('partida_id', $partida->id)->count();
        $tema_usuario = Tema::where('user_id', Auth::user()->id)->where('partida_id', $partida->id);

        $participantes_actuales = Tema::where('partida_id', $partida->id)->count();
        // Partida::where('id', $partida->id)->pluck('num_participantes');
        $porcentaje = 0;

        return view('rondas.crear', [
            'partida' => $partida,
            'porcentaje' => $porcentaje,
            'participantes_actuales' => $participantes_actuales,
            'temas_partida' => $temas_partida,
            'tema_usuario' => $tema_usuario
        ]);
    }

    public function postCrearTema(request $request, Partida $partida)
    {
        $participantes_actuales = Partida::where('id', $partida->id)->pluck('num_participantes');
        $participantes_actuales = $participantes_actuales[0];
        if ($participantes_actuales < 10) {
            // Añadir tema
            $usuario_id =  Auth::user()->id;
            $nuevo_tema = new Tema();
            $nuevo_tema->user_id = $usuario_id;
            $nuevo_tema->partida_id = $partida->id;
            $nuevo_tema->titulo = $request->titulo;
            $nuevo_tema->descripcion = $request->descripcion;

            try {
                $nuevo_tema->save();
                $participantes_actuales++;

                if (!$partida->privada && PartidaInvitacion::existeInvitacion($partida->id, $usuario_id) == 0) {
                    $invitacion = new PartidaInvitacion();
                    $invitacion->crearInvitacion($usuario_id, $partida->id, 1);
                }

                PartidaInvitacion::aceptarInvitacion( $partida->id, $usuario_id);
                
                Partida::where('id', $partida->id)->update(['num_participantes'=> $participantes_actuales]);
            } catch (Exception $ex) {
                return redirect('home')->with("mensaje","Error al añadir tema");
            }
            return redirect('home')->with("mensaje","Añadido tema conrrectamente");
        } else {
            return redirect('home')->with("mensaje","La partida ya esta completa");
        }
    }
}

