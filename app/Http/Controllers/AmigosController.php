<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Amigo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\View\View;
// use Illuminate\Support\Facades\Auth;

class AmigosController extends Controller
{

    public function getAmigos()
    {
        $user_id = Auth::user()->id;

        $amigos_usuario = $this->queryAmigosUsuario($user_id);
        $solicitudes_usuario = $this->querySolicitudesUsuario($user_id);
        // var_dump($amigos_usuario);
        // exit();
        return view('amigos.index', array('amigos_usuario' => $amigos_usuario, 'solicitudes_usuario' => $solicitudes_usuario));
    }

    public function getUsuarios()
    {
        $array_usuarios = Amigo::all()->where('user_1', $user_id);

        return view('amigos.index', array('amigos_usuario' => $array_usuarios));
    }

    public function getBusquedaUsuarios()
    {
        $user_id = Auth::user()->id;
        $solicitudes_usuario = $this->querySolicitudesUsuario($user_id);

        return view('amigos.busqueda', array('solicitudes_usuario' => $solicitudes_usuario));
    }

    public function getSolicitudes()
    {
        $user_id = Auth::user()->id;
        $solicitudes_usuario = $this->querySolicitudesUsuario($user_id);

        return view('amigos.solicitudes', array('solicitudes_usuario' => $solicitudes_usuario));
    }

    public function postAceptarSolicitud(Request $request)
    {
        $solicitud_id = $request->solicitud_id;

        Amigo::where('id',$solicitud_id)->update(['estado_solicitud'=> 0]);

        return redirect()->action([AmigosController::class, 'getSolicitudes'])->with("mensaje", "Solicitud Aceptada");
    }

    public function postEliminarSolicitud(Request $request)
    {
        $solicitud_id = $request->solicitud_id;

        Amigo::destroy($solicitud_id);

        return redirect()->action([AmigosController::class, 'getSolicitudes'])->with("mensaje", "Solicitud Eliminada");
    }

    private function querySolicitudesUsuario($user_id)
    {
        return DB::table('users as u')
            ->join('amigos as a', function($query) {
                $query
                    ->on('a.user_1_id', '=', 'u.id')
                    ->orOn('a.user_2_id', '=', 'u.id');
            })
            ->select(['u.*', 'a.id as solicitud_id', 'a.user_1_id'])
            ->where('u.id', '<>', $user_id)
            ->where('a.estado_solicitud', '<>', 0)
            ->get();
    }

    private function queryAmigosUsuario($user_id)
    {
        return DB::table('users as u')
        ->join('amigos as a', function ($query) {
            $query
                ->on('a.user_1_id', '=', 'u.id')
                ->orOn('a.user_2_id', '=', 'u.id');
        })
        ->select('u.*')
        ->where('u.id', '<>', $user_id)
        ->where('a.estado_solicitud', '<>', 1)
        ->get();
    }

    public function ajaxBuscarAmigo(Request $request)
    {
        $user_id = Auth::user()->id;
        $nombre = $request->nombre;
        $amigos_encontrados = "resultado";

        $amigos_id = DB::table('users as u')
        ->join('amigos as a', function ($query) {
            $query
                ->on('a.user_1_id', '=', 'u.id')
                ->orOn('a.user_2_id', '=', 'u.id');
        })
        ->where('u.id', '<>', $user_id)
        ->pluck('u.id')
        ->toArray();

        $amigos_encontrados = DB::table('users as u')
                                ->whereNotIn('u.id', $amigos_id)
                                ->where("u.name", "like", $nombre."%")
                                ->where('u.id', '<>', $user_id)
                                ->select(['u.name', 'u.id', 'u.avatar'])
                                ->get();


        // dd($amigos_encontrados[0]->name);
        return view('amigos.ajaxBuscarAmigo', array('usuario_encontrados' => $amigos_encontrados));
        // JSON
        // return response()->json($amigos_encontrados);
    }

    public function postPeticion(Request $request) {
        $amigo = new Amigo;

        $amigo->user_1_id = Auth::user()->id;
        $amigo->user_2_id = $request->solicitud_id;
        $amigo->estado_solicitud = 1;
        $amigo->save();

        return redirect()->action([AmigosController::class, 'getBusquedaUsuarios'])->with("mensaje", "Solicitud enviada");
    }
}
