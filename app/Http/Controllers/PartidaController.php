<?php

namespace App\Http\Controllers;

use App\Amigo;
use App\Partida;
use App\PartidaUsuario;
use App\PartidaInvitacion;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class PartidaController extends Controller
{
    // Lleva a la vista principal de '/partidas' = partidas.index
    public function getPartida()
    {
        return view('partidas.index');
    }

    // Lleva a la vista que lista las partidas del usuario, 'partidas/usuario' = partidas.list
    public function getPartidas()
    {
        $partidas = Partida::getPartidasUsuario(Auth::user()->id);
        return view('partidas.list', array('arrayPartidas' => $partidas));
    }

    public function getPartidasPublicas()
    {
        $partidas = Partida::getPartidasPublicas();
        return view('partidas.list-publicas', array('arrayPartidas' => $partidas));
    }

    public function getCrearPartida()
    {
        $amigos = Amigo::getAmigosAll(Auth::user()->id);
        return view('partidas.crear',  array('arrayAmigos' => $amigos));
    }

    public function postAmigos(Request $request) {
        $amigos = Amigo::getAmigos(Auth::user()->id, $request->busqueda);
        return response(json_encode($amigos),200)->header('Content-type', 'text/plain');
    }

    public function postSelectAmigos(Request $request) {
        $amigos = Amigo::getAmigosAll(Auth::user()->id);
        return view('partidas.crear',  array('arrayAmigos' => $amigos, 'arrayAmigosSelect' => $request->invitados));
        // ->with("mensaje", "Solicitud Eliminada");
    }

    public function postCrearPartida(Request $request)
    {
        $arrayInvitaciones =  ($request->tipoPartida == 'privada') ? explode(",", $request->amigosInvitados[0]) : [];

        if($arrayInvitaciones != NULL && count($arrayInvitaciones) >= 2 || $request->tipoPartida == 'publica') {
            $partida = new Partida();
            $partida->nombre_partida = $request->nombre;
            $partida->slug = Str::slug($request->nombre);
            $partida->codigo = $this->generarCodigo();
            $partida->privada =  ($request->tipoPartida == 'privada') ? 1 : 0;
            $partida->creado_por = Auth::user()->id;
            $partida->fecha_inicio = $request->fecha_inicio;
            $partida->num_participantes = 0; // al añadir el tema se crea
            $partida->duracion = '00:00:00';
            $partida->estado_id = 2;
            
            try {
                $partida->save();

                array_push($arrayInvitaciones, Auth::user()->id);
                $array_usuarios = $arrayInvitaciones;
    
                $invitacion = new PartidaInvitacion();
                $invitacion->crearInvitaciones($array_usuarios, $partida->id);

                return redirect('home')->with("mensaje", "Partida creada con exito!");
            } catch (Exception $ex) {
                return redirect('home')->with("error", "Error al crear la partida");
            }
        } else {
            return redirect('partidas/crear')->with("error", "Error al invitar, seleccione minimo a 2 personas");
        }
    }

    //Funcion generar codigo de partida aleatorio
    public function generarCodigo()
    {
        $codigo = '';
        $patron = '1234567890abcdefghijklmnopqrstuvwxyz';
        $max = strlen($patron) - 1;

        // Genera un codigo de manera aleatoria
        for ($i = 0; $i < 4; $i++) {
            $codigo .= $patron[mt_rand(0, $max)];
        }

        // comprobar si existe
        if (Partida::where('codigo', '=', $codigo)->exists()) {
            return $this->generarCodigo();
        }

        return $codigo;
    }

    public function crearRelacionPartidaUsuario($id_partida, $id_usuario)
    {
        $relacion = new PartidaUsuario();
        $relacion->partida_id = $id_partida;
        $relacion->user_id = $id_usuario;

        try {
            $relacion->save();
            return true;
        } catch (Exception $ex) {
            return false;
        }
        return view('partidas.crear');
    }

    public function getInvitaciones() {
        $invitaciones = new PartidaInvitacion();
        $invitaciones = $invitaciones->getInvitaciones(Auth::user()->id);

        return view('partidas.invitaciones', array('invitaciones' => $invitaciones));
    }

}
