<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Ronda;
use App\Dibujo;
use App\Partida;
use App\PartidaUsuario;
use App\RondaUsuario;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class DibujosController extends Controller
{

	public function getVer($id)
	{
        $dibujoID = Dibujo::findOrFail($id);

		return view('dibujos.mostrar',  array('dibujoSeleccionado' => $dibujoID));
	}

	public function getSubirDibujos(Partida $partida, Ronda $ronda)
	{	
		return view('dibujos.index', array('ronda' => $ronda));
	}

	public function getVerDibujosRonda(Request $request, Partida $partida, Ronda $ronda)
	{	
		if ($request->estado_id == 2 || $request->estado_id == 6 ) {
			return view('rondas.index',  array('arrayRondas' => $partida->rondas, 'partida' => $partida));
		}
        $dibujos = Dibujo::all()->where('ronda_id', $ronda->id);
        $usuario_id = Auth::user()->id;
        $voto_usuario = DB::table('rondas_usuarios')->where('ronda_id', $ronda->id)->where('user_id', $usuario_id)->first();
		$dibujoSubido = Dibujo::dibujoSubido(Auth::user()->id, $ronda->id);	
		$ganador = null;

		if ($ronda->estado_id == 5) {
			$ganador = RondaUsuario::getGanador($ronda->id);
			$ganador = $ganador[0];
		}

		$anonimo = RondaUsuario::noEsAnonimo($usuario_id, $ronda->id);

		return view('dibujos.list', array(
			'usuario_id' =>  $usuario_id,
			'ronda' => $ronda,
            'arrayDibujos' => $dibujos,
            'voto_usuario' => $voto_usuario,
			'dibujoSubido' => $dibujoSubido,
			'ganador' => $ganador,
			'anonimo' => $anonimo
		));
	}
	
	public function getPizarra(Partida $partida, Ronda $ronda)
	{
		return view('dibujos.pizarra', array('ronda' => $ronda));
	}

	public function getCrear(Partida $partida, Ronda $ronda)
	{	
		return view('dibujos.crear', array('ronda' => $ronda));
	}

	public function postCrear(Request $request, Partida $partida ,Ronda $ronda)
	{
		if ($request->file('dibujo') == NULL) {
			return redirect("perfil")->with("mensaje", "Seleccione un dibujo");
		} else {

			$estado_dibujo = Dibujo::getDibujo(Auth::user()->id,$ronda->id);
			$dibujo = ($estado_dibujo == null) ? new Dibujo() : $estado_dibujo;

			$dibujo->user_id = Auth::user()->id;
			$dibujo->ronda_id = $ronda->id;
			$dibujo->fecha = date("Y-m-d");
			$dibujo->votos = 0;
	
			$nombre_dibujo = $request->file('dibujo')->getClientOriginalName();
			$dibujo->imagen = $request->file('dibujo')->storeAs('public', $nombre_dibujo);
	
			try {
				$dibujo->save();
				return redirect("perfil")->with("mensaje", "Dibujo subido con exito!");
			} catch (Exception $ex) { // \Illuminate\Database\QueryException
				return redirect("perfil")->with("mensaje", "Fallo al subir el dibujo");
			}
	
			return view('dibujos.crear');
		}
    }

	public function postVotos(Request $request, Partida $partida, Ronda $ronda)
	{
        $usuario_id = Auth::user()->id;	
		if (RondaUsuario::noEsAnonimo($usuario_id, $ronda->id) == 0) {
			RondaUsuario::votoAnonimo($ronda->id, $usuario_id);
		}

		try {
            $voto_usuario = DB::table('rondas_usuarios')
            ->where('user_id', $usuario_id)
            ->where('ronda_id', $ronda->id)
            ->update(['dibujo_voto' => $request['dibujoVoto']]);

            DB::table('dibujos')->where('id', $request['dibujoVoto'])->increment('votos', 1);

			return redirect()->route('dibujos.listado', [$partida, $ronda])->with("mensaje", "Voto realizado con exito!");
		} catch (Exception $ex) { // \Illuminate\Database\QueryException
			return view("dibujos.index")->with("mensaje", "Fallo votar");
		}
	}

	public function getGanador(Partida $partida, Ronda $ronda)
	{	
		// dd('hola');
		$dibujos = Dibujo::where('ronda_id', $ronda->id)->orderBy('votos', 'desc')->get();
		$votosMax = $dibujos[0]->votos;
		// dd($dibujos);
		return view("dibujos.ganador", array('ronda' => $ronda, 'dibujos' => $dibujos, 'votosMax'=> $votosMax));

	}
}
