<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dibujo extends Model
{
    public function rondausuario()
    {
        return $this->hasOne(RondaUsuario::class);
    }

    public function autor($user_id)
    {
        return User::where('id', $user_id)->first();
    }
    
    public static function getDibujo($user_id, $ronda_id) {
        return Dibujo::where('user_id', $user_id)->where('ronda_id', $ronda_id)->first();
    }

    public static function actualizarVotos($dibujo_id, $ronda_id, $num_votos)
    {
        Dibujo::where('ronda_id', $ronda_id)->where('id', $dibujo_id)->update(['votos' => $num_votos]);
    }

    public static function actualizarPuntos($ronda_id, $user_id, $puntos) {
        $dibujo = Dibujo::where('ronda_id', $ronda_id)
                        ->where('user_id',$user_id)
                        ->update(['puntos' => $puntos]);
    }

    public static function dibujoSubido($user_id, $ronda_id) {
        return Dibujo::where('ronda_id', $ronda_id)->where('user_id', $user_id)->count();
    }
}
