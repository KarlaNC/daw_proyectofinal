<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class PartidaInvitacion extends Model
{
    protected $table = 'partidas_invitaciones';

    // Función para crear las partidas

    public function partida(){
        return $this->belongsTo(Partida::class);
    }

    public function estado(){
        return $this->belongsTo(EstadoPartida::class);
    }

    public function crearInvitacion($user_id, $partida_id, $estado_id = 2) {
        $invitacion = new PartidaInvitacion();
        $invitacion->partida_id = $partida_id;
        $invitacion->user_id = $user_id;
        $invitacion->estado_id = $estado_id;
        $invitacion->save();
    }

    public function crearInvitaciones($array_usuarios, $partida_id) {
        foreach ($array_usuarios as $usuario_id) {
            $this->crearInvitacion($usuario_id ,$partida_id);
        }
    }

    public function getInvitaciones($user_id) {
        $invitaciones = PartidaInvitacion::join('partidas as p','p.id','partidas_invitaciones.partida_id' )
        ->where('p.estado_id', 2)
        ->where('partidas_invitaciones.estado_id', 2)
        ->where('user_id', $user_id)->get();

        return $invitaciones;
        // TODO: return PartidaInvitacion::where('user_id', $user_id)->get();
    }

    public static function existeInvitacion($partida_id, $user_id) {
        return PartidaInvitacion::where('partida_id', $partida_id)->where('user_id', $user_id)->count();
    }

    public static function getNumInvitaciones() {
        $partida_invitacion = new PartidaInvitacion();
        return $partida_invitacion->getInvitaciones(Auth::user()->id)->count();
    }

    public static function aceptarInvitacion($partida_id, $user_id) {
        PartidaInvitacion::where('partida_id', $partida_id)->where('user_id', $user_id)->update(['estado_id' => 1]);
    }

    public static function cancelarInvitacion($partida_id, $user_id) {
        PartidaInvitacion::where('partida_id', $partida_id)->where('user_id', $user_id)->update(['estado_id' => 4]);
    }

    public static function getPartidasUsuario($user_id) {
        return PartidaInvitacion::select('partida_id')->where('user_id', $user_id)->get();
    }
}
