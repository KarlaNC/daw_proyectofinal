<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RondaUsuario extends Model
{
    protected $table = 'rondas_usuarios';
    protected $primaryKey =  ['ronda_id', 'user_id'];
    public $incrementing = false;

    /**
     * Set the keys for a save update query.
     *
     * @param Builder $query
     * @return Builder
     */
    protected function setKeysForSaveQuery($query)
    {
        $keys = $this->getKeyName();
        if(!is_array($keys)){
            return parent::setKeysForSaveQuery($query);
        }

        foreach($keys as $keyName){
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if(is_null($keyName)){
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }

    public function ronda()
    {
        return $this->hasOne(Ronda::class);
    }

    public function usuario()
    {
        return $this->hasOne(RondaUsuario::class);
    }

    public function crearRondasUsuarios($ronda_id, $user_id, $partida_id) 
    {  
        // Un rondausuario por ronda
        $usuarios = Tema::where('partida_id', $partida_id)->get();
        foreach ($usuarios as $usuario) {
            $rondas_usuarios = New RondaUsuario();
            $rondas_usuarios->ronda_id = $ronda_id;
            $rondas_usuarios->user_id = $usuario->user_id;
            $rondas_usuarios->save();
        }
    }
    
    // Recuento de 1 Ronda
    public function getRecuentosVotos($partida_id, $ronda_id) {
        $num_votos = DB::raw('count(*) as num_votos');
        $votos_dibujos_ronda = RondaUsuario::select('dibujo_voto', $num_votos)
                                            ->where('ronda_id', $ronda_id)
                                            ->groupBy('dibujo_voto')
                                            ->get();

        foreach ($votos_dibujos_ronda as $voto_dibujo) {
            Dibujo::actualizarVotos($voto_dibujo['dibujo_voto'], $ronda_id, $voto_dibujo['num_votos']); // Actualiza los votos en dibujo
        }
        $this->actualizarPuntuacion($partida_id, $ronda_id);
    }

    public function actualizarPuntuacion($partida_id, $ronda_id) {
        dump('Se actualiza la puntuacion');
        $participantes = Dibujo::where('ronda_id', $ronda_id)
                                                ->orderByDesc('votos')
                                                ->get();
        $max_votos = $participantes->first()->votos;

        $ganadores = Dibujo::where('ronda_id', $ronda_id)
                    ->where('votos', $max_votos)->get();

        if ($ganadores->count() == 1) {
            dump('Hay un unico ganador');
            // Actualizar datos
            $this->actualizarGanador($partida_id, $ronda_id, $ganadores->first()->user_id);
            $participantes =  $participantes->diff($ganadores); // Colección con los participantes - ganador
            PartidaUsuario::actualizarGanador($partida_id, $ganadores->first()->user_id);
        } else {
            dump('hay más de 1 ganador == Empate');
            // Actualizar datos
            $this->actualizarEmpate($partida_id, $ronda_id, $ganadores);
            $participantes =  $participantes->diff($ganadores); // Colección con los participantes - ganador
            PartidaUsuario::actualizarGanador($partida_id, $ganadores->first()->user_id);
        }
        
        $this->actualizarJugadores($partida_id, $ronda_id, $participantes);
    }

    function actualizarGanador($partida_id, $ronda_id, $user_id) {
        // Actualizar los votos del ganador y del resto de jugadores
        $puntos = $this->getPuntos(4);
        $this->actualizarRondaUsuario($ronda_id, $user_id, $puntos);
        Dibujo::actualizarPuntos($ronda_id, $user_id, $puntos);
        PartidaUsuario::actualizarPuntos($partida_id, $user_id, $puntos);
    }

    function actualizarEmpate($partida_id, $ronda_id, $ganadores) {
        // Actualizar los vatos de los ganadores y del resto de jugadores
        $puntos = $this->getPuntos(3);

        foreach ($ganadores as $ganador) {
            dump("Empate con id : " .  $ganador->user_id);
            $this->actualizarRondaUsuario($ronda_id, $ganador->user_id, $puntos);
            Dibujo::actualizarPuntos($ronda_id, $ganador->user_id, $puntos);
            PartidaUsuario::actualizarPuntos($partida_id, $ganador->user_id, $puntos);
        }
    }

    function actualizarJugadores($partida_id, $ronda_id, $jugadores) {
        // Actualizar los vatos de los ganadores y del resto de jugadores
        dump('Se actualizan las puntuaciones de todos los jugadores que no han ganado');
        $puntos = $this->getPuntos(2);

        foreach ($jugadores as $jugador) {
            dump("Jugador con id: " . $jugador->user_id);
            $this->actualizarRondaUsuario($ronda_id, $jugador->user_id, $puntos);
            Dibujo::actualizarPuntos($ronda_id, $jugador->user_id, $puntos);
            PartidaUsuario::actualizarPuntos($partida_id, $jugador->user_id, $puntos);
        }
    }

    function actualizarRondaUsuario($ronda_id, $user_id, $puntos) 
    {
        $ronda_usuario = RondaUsuario::where('ronda_id', $ronda_id)
                                        ->where('user_id',$user_id)
                                        ->update(['ronda_usuario_puntos' => $puntos]);
    }

    function getPuntos($id) 
    {
        return Puntuacion::where('id', $id)->value('puntos');
    }

    public static function getGanador($ronda_id) {
        return RondaUsuario::where('ronda_id', $ronda_id)->orderBy('ronda_usuario_puntos', 'desc')->get();
    }

        
    public static function noEsAnonimo($user_id, $ronda_id) {
        return RondaUsuario::where('user_id', $user_id)->where('ronda_id', $ronda_id)->count();
    }

    public static function votoAnonimo($ronda_id, $usuario_id) {
        $anonimo = new RondaUsuario();
        $anonimo->ronda_id = $ronda_id;
        $anonimo->user_id = $usuario_id;
        $anonimo->ronda_usuario_puntos = 0;
        $anonimo->save();
    }
}
