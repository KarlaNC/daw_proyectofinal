<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Amigo extends Model
{
    public static function getAmigosAll($user_id) {
        return DB::table('users as u')
        ->join('amigos as a', function ($query) {
            $query
                ->on('a.user_1_id', '=', 'u.id')
                ->orOn('a.user_2_id', '=', 'u.id');
        })
        ->select('u.*')
        ->where('u.id', '<>', $user_id)
        ->where('a.estado_solicitud', '<>', 1)
        ->get();
    }

    public static function getAmigos($user_id, $string) {
        return DB::table('users as u')
        ->join('amigos as a', function ($query) {
            $query
                ->on('a.user_1_id', '=', 'u.id')
                ->orOn('a.user_2_id', '=', 'u.id');
        })
        ->select('u.*')
        ->where('u.id', '<>', $user_id)
        ->where('a.estado_solicitud', '<>', 1)
        ->where('u.nick', 'LIKE', '%'. $string .'%')
        ->get();
    }
}
