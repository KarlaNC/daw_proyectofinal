<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // sftp://Inori@synserver.duckdns.org/wwwKarla/public_html
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Pasa a cualquier vista, desde su inicio, las varibales de acción y controlador
        app('view')->composer('layouts.master', function ($view) {
            $action = app('request')->route()->getAction();

            $controller = class_basename($action['controller']);

            list($controller, $action) = explode('@', $controller);

            $controller = substr($controller, 0, -10);

            $view->with(compact('controller', 'action'));
        });
    }
}
