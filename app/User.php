<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'nick', 'email', 'password', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public static function getPuntos($user_id) {
        return PartidaUsuario::where('user_id', $user_id)->sum('partida_puntos');
    }

    public static function getLvL($puntos) {
        $nivel = $puntos/10;
        return $nivel;
    }
    
    public function rondas()
    {
        return $this->belongsToMany(Ronda::class)
                        ->using('App\Models\RondaUsuario')
                        ->withPivot([
                            'votado',
                            'created_at',
                            'updated_at',
                        ]);
    }

    public function numAmigos() {
        return Amigo::getAmigosAll($this->id)->count();
    }
}
