<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sluggable;
// use Cviebrock\EloquentSluggable\Sluggable;

class Partida extends Model
{
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function estado()
    {
        return $this->belongsTo(EstadoPartida::class);
    }

    public function rondas()
    {
        return $this->hasMany(Ronda::class);
    }

    public static function getPartidasUsuario($user_id) {
        $array_partidas1 = PartidaUsuario::getPartidasUsuario($user_id);
        $array_partidas2 = PartidaInvitacion::getPartidasUsuario($user_id);
        return Partida::whereIn('id', $array_partidas1)->orWhereIn('id', $array_partidas2)->where('estado_id','<>',4)->get();
    }

    public static function getPartidasPublicas() {
        return Partida::where('estado_id', 2)->where('privada', 0)->orWhere('estado_id', 1)->where('privada', 0)->get();
    }

    public function crearPartida($fecha_actual)
    {
        $array_partidas = Partida::all()->where('fecha_inicio', $fecha_actual)
        ->where('estado_id', 2)
        ->where('num_participantes', '<=', 10);
        
        
        foreach ($array_partidas as $partida) {
            if ($partida['num_participantes'] >= 3 ) {
                try {
                    $ronda = new Ronda();
                    $ronda->crearRondas($partida['id']);
                    $this->setFechaFin($partida['id']);
                    $this->activarPartida($partida['id']);
                } catch (\Throwable $th) {
                    dump('Error al crear la partida');
                }
            } else {
                $this->cancelarPartida($partida['id']);
            }
        }
    }
    
    public function activarPartida($partida_id)
    {
        Partida::where('id', $partida_id)->update(['estado_id' => 1]);
    }

    public function cancelarPartida($partida_id)
    {
        Partida::where('id', $partida_id)->update(['estado_id' => 4]);
    }

    public function completarPartida($partida_id)
    {
        Partida::where('id', $partida_id)->update(['estado_id' => 3]);
    }
    
    public function setFechaFin($id)
    {
        $ultima_ronda = Ronda::where('partida_id', $id)->orderBy('fecha_fin', 'desc')->first();
        $fecha_fin = $ultima_ronda['fecha_fin'];
        Partida::where('id', $id)->update(['fecha_fin' => $fecha_fin]);
    }
}
