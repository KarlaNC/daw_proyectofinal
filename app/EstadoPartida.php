<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoPartida extends Model
{
    protected $table = 'estados_partidas';
}
