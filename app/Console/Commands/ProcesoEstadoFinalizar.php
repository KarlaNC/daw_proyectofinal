<?php

namespace App\Console\Commands;

use App\Ronda;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ProcesoEstadoFinalizar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procesos:estado_finalizar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $rondas = Ronda::where('estado_id', 4)->get();
        foreach ($rondas as $ronda) {
            $fecha_votos = Carbon::createFromDate($ronda->fecha_votos);
            $ronda->fecha_inicio = $fecha_votos->copy()->subDays(4);
            $ronda->fecha_votos = $fecha_votos->copy()->subDay(1);
            $ronda->fecha_fin = $fecha_votos;
            $ronda->save();
        }
    }
}
