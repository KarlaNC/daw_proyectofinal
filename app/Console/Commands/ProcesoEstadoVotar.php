<?php

namespace App\Console\Commands;

use App\Ronda;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ProcesoEstadoVotar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procesos:estado_votar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $rondas = Ronda::where('estado_id', 3)->get();
        // dd($rondas);
        foreach ($rondas as $ronda) {
            $fecha_inicio = Carbon::createFromDate($ronda->fecha_inicio);
            $ronda->fecha_inicio = $fecha_inicio->copy()->subDays(3);
            $ronda->fecha_votos = $fecha_inicio;
            $ronda->fecha_fin = $fecha_inicio->copy()->addDay();
            $ronda->save();
        }
    }
}
