<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Partida;

class ProcesosCrearPartida extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procesos:crear_partida';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fecha_actual = now()->format('Y-m-d');

        $partida = new Partida();
        $partida->crearPartida($fecha_actual);
        // Controlar que los temas es igual al numero de participantes
        // Añadir lo de cancelar automaticamente las partidas si no llegan al número
    }
}
