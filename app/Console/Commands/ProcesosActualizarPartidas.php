<?php

namespace App\Console\Commands;

use App\Ronda;
use Illuminate\Console\Command;

class ProcesosActualizarPartidas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature =  'procesos:actualizar_partida';

    /**
     * The console command description.
     *
     * @var string
     */
    // TODO: escribir la desprición
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fecha_actual = now()->format('Y-m-d');

        $ronda = new Ronda();
        $ronda->actualizarRondasVotar($fecha_actual); // actualizarRondasVotar - actualizarRondasFinalizar
        $ronda->actualizarRondasFinalizar($fecha_actual); // Cambio de estado (Votando > Completada) + Recuento de votos
    }
}
