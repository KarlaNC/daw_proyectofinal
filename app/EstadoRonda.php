<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoRonda extends Model
{
    protected $table = 'estados_rondas';
}
