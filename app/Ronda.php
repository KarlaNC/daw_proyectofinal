<?php

namespace App;

use Carbon\CarbonImmutable;
use DateInterval;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Ronda extends Model
{
    public function getRouteKeyName()
    {
        return 'slug';
    }
    
    public function partida()
    {
        return $this->belongsTo(Partida::class);
    }

    public function estado()
    {
        return $this->belongsTo(EstadoRonda::class);
    }

    public function tema()
    {
        return $this->belongsTo(Tema::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class)
                        ->using('App\Models\RondaUsuario')
                        ->withPivot([
                            'votado',
                            'created_at',
                            'updated_at',
                        ]);
    }

    /**
     *  $array_temas, los temas ordenados de manera aleatoria
     *  $fecha, la fecha actual
     *  Mediante un blucle de los temas, se llama a la funcion crearRondas (Donde se crearán en la BBDD dichas rondas)
     *  La fecha se irá actualizando con la función crearronda
     */
    public function crearRondas($partida_id)
    {
        $array_temas = Tema::where('partida_id', $partida_id)->inRandomOrder()->get();
        $fecha = CarbonImmutable::now();
        $partida_usuario = new PartidaUsuario();
        
        foreach ($array_temas as $tema_rand) {
            $this->crearRonda($partida_id, $tema_rand->id,$tema_rand->titulo, $tema_rand['user_id'], $fecha, $fecha->addDay(3), $fecha->addDay(4));
            $partida_usuario->crearPartidaUsuario($partida_id, $tema_rand['user_id']); // Se crea PartidaUsuario
            $fecha =  $fecha->addDay(4);
        }
    }

    private function crearRonda($partida_id, $tema_rand_id, $tema_titulo, $usuario_id, $fecha_inicio, $fecha_votos, $fecha_fin)
    {   
        // Creación de la ronda
        $nueva_ronda = new Ronda();
        $nueva_ronda->partida_id = $partida_id;
        $nueva_ronda->tema_id = $tema_rand_id;
        $nueva_ronda->slug = Str::slug($tema_titulo);
        $nueva_ronda->fecha_inicio = $fecha_inicio;
        $nueva_ronda->fecha_votos = $fecha_votos;
        $nueva_ronda->fecha_fin = $fecha_fin;
        $nueva_ronda->estado_id = 2;
        $nueva_ronda->save();

        // Relación ronda_usuario (1 RondaUsuario por cada usuario)
        $rondaUsuario = new RondaUsuario();
        $rondaUsuario->crearRondasUsuarios($nueva_ronda->id, $usuario_id, $partida_id);

        // Si la fecha de inicio de la ronda == partida, se cambia de estado a 3 - 'Dibujando'
        if (CarbonImmutable::now()->month == $fecha_inicio->month && CarbonImmutable::now()->day == $fecha_inicio->day) {
            $this->rondaDibujando($nueva_ronda->id);
        }
    }

    public function actualizarRondasVotar($fecha_actual)
    {
        // Rondas que estan en curso  => Dibujando - Votando
        $ronda_votos = Ronda::all()->where('fecha_votos', $fecha_actual)
                                   ->where('estado_id', 3);

        foreach ($ronda_votos as $ronda) {
            Ronda::where('id', $ronda->id)->update(['estado_id'=> 4]);
        }
    }

    public function actualizarRondasFinalizar($fecha_actual)
    {
        $rondas_completas = Ronda::all()->where('fecha_fin', $fecha_actual)
                                        ->where('estado_id', 4);
        foreach ($rondas_completas as $ronda) {
            $partida = Partida::find($ronda['partida_id']);
            
            if ($fecha_actual <= $partida->fecha_fin) {
                $ronda_usuario = new RondaUsuario();
                $ronda_usuario->getRecuentosVotos($ronda['partida_id'], $ronda['id']);               
                $this->rondaCompleta($ronda->id);
                $this->rondaDibujando($ronda->id + 1);
            } 
        }
    }

    private function rondaActiva($ronda_id) {
        Ronda::where('id', $ronda_id)->update(['estado_id'=> 1]);
    }

    private function rondaCompleta($ronda_id) {
        Ronda::where('id', $ronda_id)->update(['estado_id'=> 5]);
    }

    private function rondaDibujando($ronda_id) {
        Ronda::where('id', $ronda_id)->update(['estado_id'=> 3]);
    }
}
