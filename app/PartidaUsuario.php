<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class PartidaUsuario extends Model
{

    protected $table = 'partidas_usuarios';
    protected $primaryKey =  ['partida_id', 'user_id'];
    public $incrementing = false;

    public function crearPartidaUsuario($partida_id, $usuario_id) {
        $nueva_ronda = new PartidaUsuario();
        $nueva_ronda->partida_id = $partida_id;
        $nueva_ronda->user_id = $usuario_id;
        $nueva_ronda->partida_puntos = 0;
        $nueva_ronda->save();
    }

    public static function actualizarPuntos($partida_id, $user_id, $puntos) {
        $puntos_partida = PartidaUsuario::where('partida_id', $partida_id)->where('user_id', $user_id)->pluck('partida_puntos')[0];

        PartidaUsuario::where('partida_id', $partida_id)
                        ->where('user_id', $user_id)
                        ->update(['partida_puntos' => $puntos_partida + $puntos]);
    }

    public static function getPartidasUsuario($user_id) {
        return PartidaUsuario::select('partida_id')->where('user_id', $user_id)->get();
    }

    // public static function actualizarGanadores($partida_id, $ganadores) {
    //     $ganadores_id = '';
    //     foreach ($ganadores as $ganador) {
    //         $ganadores_id .= '' . $ganador->id;
    //     }

    //     PartidaUsuario::where('partida_id',$partida_id)->update(['ganador' =>  $ganadores_id]);
    // }

    public static function actualizarGanador($partida_id, $user_id) {
        PartidaUsuario::where('partida_id',$partida_id)->update(['ganador' =>  $user_id]);
    }
}
